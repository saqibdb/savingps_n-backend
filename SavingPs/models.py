from django.db import models
from django.db.models.fields import related
from django.db.models.fields.related import RelatedField
from colorfield.fields import ColorField
from django_pandas.io import read_frame
from fbprophet import Prophet
import pandas as pd
import json
from datetime import datetime, timedelta
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class User(models.Model):

    firstName = models.CharField(blank=True, max_length=100)
    surname = models.CharField(blank=True, max_length=100)
    email = models.EmailField(unique=True)
    dob = models.DateField(null=True, blank=True)
    phone = models.CharField(unique=True, max_length=256)
    postCode = models.CharField(max_length=256, blank=True, null=True)
    address = models.TextField(max_length=256, blank=True, null=True)
    profilePicture = models.ImageField(null=True, blank=True)
    verificationCodePhone = models.CharField(blank=True, max_length=10)
    isPhoneVerified = models.BooleanField(default=False)
    isAccountBalanceShow = models.BooleanField(default=True)
    isPotBalanceShow = models.BooleanField(default=True)
    authToken = models.CharField(blank=True, max_length=101)
    pin = models.CharField(blank=True, max_length=10)

    accessToken = models.TextField(null=True, blank=True)
    accessTokenExpiration = models.DateTimeField(null=True, blank=True)
    refreshToken = models.CharField(max_length=100, null=True, blank=True)
    tokenType = models.CharField(max_length=20, null=True, blank=True)
    deviceToken = models.CharField(max_length=500, null=True, blank=True)

    nationality = models.CharField(blank=True, max_length=100)
    isBankConnected = models.BooleanField(default=False)

    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)
    lastLoginDate = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)



class Bank(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    accountId = models.CharField(max_length=100, null=True)
    currency = models.CharField(max_length=10, null=True)
    accountNumber = models.CharField(max_length=100, null=True)
    bankName = models.CharField(max_length=103, null=True)
    lastUpdationDate = models.DateTimeField(auto_now_add=True, null=True)


    def __str__(self):
        return self.bankName

class Card(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    accountId = models.CharField(max_length=100, null=True)
    currency = models.CharField(max_length=10, null=True)
    cardNumber = models.CharField(max_length=100, null=True)
    cardName = models.CharField(max_length=104, null=True)
    cardProvider = models.CharField(max_length=105, null=True)

    cardNetwork = models.CharField(max_length=106, null=True)
    cardType = models.CharField(max_length=107, null=True)

    lastUpdationDate = models.DateTimeField(auto_now_add=True, null=True)







class Category(models.Model):

    name = models.CharField(max_length=100, null=True)
    categoryImage = models.ImageField(null=True, blank=True)

    initialColor = ColorField(default='#FF0000')
    middleColor = ColorField(default='#00FF00')
    finalColor = ColorField(default='#0000FF')

    isDuplicateable = models.BooleanField(default=False)

    def __str__(self):
        return str(self.name)

class Subcategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, null=True)

    def __str__(self):
        return str(self.name)


class Vendor(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=108, null=True)
    isRetailer = models.BooleanField(default=False)

    class Meta:
        indexes = [
            models.Index(fields=['name', ]),
        ]
        ordering = ('name',)


    def __str__(self):
        return str(self.name)




class Transaction(models.Model):
    category = models.ForeignKey(Category, on_delete=models.SET_NULL, null=True)
    subcategory = models.ForeignKey(Subcategory, on_delete=models.SET_NULL, null=True)
    vendor = models.ForeignKey(Vendor, on_delete=models.SET_NULL, null=True)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE, null=True)
    card = models.ForeignKey(Card, on_delete=models.CASCADE, null=True)

    timestamp = models.DateTimeField(null=True, blank=True)
    description = models.TextField(max_length=256)
    transactionType = models.CharField(max_length=20, null=True, blank=True)
    merchantName = models.TextField(max_length=256, null=True)
    amount = models.FloatField(default=0)
    currency = models.CharField(max_length=10, null=True)
    transactionId = models.CharField(max_length=100, null=True)
    metaMerchantName = models.TextField(max_length=256, null=True)
    metaProviderCategory = models.CharField(max_length=100, null=True)
    transactionClassificationString = models.CharField(max_length=109, null=True)

    creationDate = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    class Meta:
        indexes = [
            models.Index(fields=['transactionId', 'user']),
        ]

    def __str__(self):
        return str(self.id)

    def get_prediction(user):
        class ProcessBS(object):
            bs=None

            #bs = pd.read_json(bs_path)
            #bs = pd.json_normalize(bs['results'])
            def __init__(self, user):

                bs =  read_frame(Transaction.objects.filter(
                    user=user
                ))
                bs['date'] = pd.to_datetime(bs['timestamp'])

                bs['date'] = bs['date'].dt.tz_convert(None)
                credit_mask = bs['amount'] > 0

                bs['credit'] = bs[credit_mask]['amount']
                bs['debit'] = bs[~credit_mask]['amount']
                del credit_mask
                #remove the mask
                bs = bs[['credit','debit','date']]
                bs.set_index('date',inplace=True)
                #fill NaNs with zeros for ease of computation
                bs.fillna(0,inplace=True)

                # convert debits from negative values to positive values
                bs['debit'] = abs(bs['debit']).astype(float)
                bs['credit'] = bs['credit'].astype(float)

                bs = bs.resample('M').mean()
                bs.fillna(0,inplace=True)
                self.bs = bs


        class GetPrediction(ProcessBS):
            def __init__(self, user):
                super().__init__(user)

            def getnextmonthsavings(self):
                current_mode = 'mid' #get this input from the user
                savemodes = {
                  "low": 0.25,
                  "mid": 0.50,
                  "high": 0.75
                } #get input from user. One of 'low', 'mid', 'high'
                # self.bs.set_index('date').resample('M').mean()
                # print(self.bs)
                if(self.bs.shape[0] < 3):
                    response = 'Insufficient data to compute predictions'
                    return response

                # if(self.bs['credit'][(self.bs.shape[0] - 1)] < self.bs['credit'][(self.bs.shape[0] - 1) - 3:(self.bs.shape[0] - 1)].mean()):
                #     latest_month = self.bs.index[(self.bs.shape[0] - 1)].strftime("%B")
                #     response = "Your average income in the last three months is less than "+latest_month + "'s income."
                #     return response

                self.bs.reset_index(level=0, inplace=True)
                bs = self.bs
                #rename columns as needed by prediction AI
                bs = bs.rename(columns = {'date': 'ds', 'debit': 'y'})

                bs = bs[['ds', 'y']]
                model = Prophet(yearly_seasonality=False, weekly_seasonality=False,interval_width=0.95).fit(bs)
                fcst = model.make_future_dataframe(1, freq = "M") #forecast 1 month ahead
                fcst = model.predict(fcst)

                #current months average income minus current months predicted spend
                response = abs(self.bs['credit'][self.bs.shape[0] - 1] - fcst['yhat'][fcst.shape[0] - 1])

                #save based on user's savemode
                response = savemodes.get(current_mode) * response

                return response
        return GetPrediction(user).getnextmonthsavings()







class Pot(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    potName = models.TextField(max_length=256, null=True)
    targetAmount = models.FloatField(default=0.0)
    potPicture = models.ImageField(null=True, blank=True)
    lockForThreeMonths = models.BooleanField(default=False)


    potStartingDate = models.DateTimeField(null=True, blank=True)
    isCompleted = models.BooleanField(default=False)


    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return str(self.id)



class Constant(models.Model):
    key = models.CharField(blank=True, max_length=100)
    text = models.TextField(max_length=256, null=True)

    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)


class Key(models.Model):
    key = models.CharField(blank=True, max_length=100)
    text = models.TextField(max_length=256, null=True)

    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)


class Tip(models.Model):
    text = models.TextField(max_length=256, null=True)

    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)




class PotTransaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    pot = models.ForeignKey(Pot, on_delete=models.CASCADE, null=True)

    amount = models.FloatField(default=0.0)
    transactionDate = models.DateTimeField(null=True, blank=True)

    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return str(self.id)



class MonthlyDebit(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    income = models.FloatField(default=0.0)
    currentDate = models.DateTimeField(null=True, blank=True)

    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return str(self.id)


class MonthlyDebitCategory(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    monthlyDebit = models.ForeignKey(MonthlyDebit, on_delete=models.CASCADE, null=True)

    targetAmount = models.FloatField(default=0.0)

    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return str(self.id)


class MonthlyCredit(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    creditName = models.TextField(max_length=256, null=True)   
    amountLeft = models.FloatField(default=0.0)
    currentDate = models.DateTimeField(null=True, blank=True)

    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return str(self.id)


class Ubuntu(models.Model):

    numberOfParticipants_validators = [
        MaxValueValidator(5),
        MinValueValidator(1)
    ]

    creator = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    name = models.CharField(max_length=500)

    savingGoal  = models.FloatField(default=0.0)
    monthlyContribution  = models.FloatField(default=0.0)
    numberOfParticipants   = models.IntegerField(default=1, null=False, validators=numberOfParticipants_validators)
    isStarted = models.BooleanField(default=False)
    date = models.DateTimeField(null=True, blank=True)


    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)
    startDate = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    def is_member(self, user):
        return UbuntuMember.objects.filter(ubuntu=self, user=user).exists()

    def is_joined(self, user):
        return UbuntuInvite.objects.filter(ubuntu=self, user=user, status="joined").exists()



    def __str__(self):
        return str(self.id)


class UbuntuPhoneNumberInvite(models.Model):
    phoneNumber = models.CharField(max_length=500)
    username = models.CharField(max_length=500)
    ubuntu = models.ForeignKey(Ubuntu, on_delete=models.CASCADE, null=True)

    status = models.CharField(max_length=100, default='invited', choices=[('invited', 'Invited'), ('joined', 'Joined')])

    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return str(self.id)

class UbuntuInvite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    ubuntu = models.ForeignKey(Ubuntu, on_delete=models.CASCADE, null=True)

    status = models.CharField(max_length=100, default='invited', choices=[('invited', 'Invited'), ('joined', 'Joined'), ('rejected', 'Rejected')])

    creationDate = models.DateTimeField(auto_now_add=True)
    updationDate = models.DateTimeField(auto_now_add=True)

    def username(self):
        return f"{self.user.firstName} {self.user.surname}"


    def __str__(self):
        return str(self.id)



class UbuntuMember(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    ubuntu = models.ForeignKey(Ubuntu, on_delete=models.CASCADE, null=True)

    index = models.IntegerField(blank=True, null=True)


    def __str__(self):
        return str(self.id)




class UbuntuShiftRequest(models.Model):
    sender = models.ForeignKey(UbuntuMember, on_delete=models.CASCADE, null=True, related_name="sender")
    receiver = models.ForeignKey(UbuntuMember, on_delete=models.CASCADE, null=True, related_name="receiver")
    
    ubuntu = models.ForeignKey(Ubuntu, on_delete=models.CASCADE, null=True)
    status = models.CharField(max_length=500, default="pending", choices=(("pending", "Pending"), ("accepted", "Accepted"), ("rejected", "Rejected"),))

    sent = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)



class UbuntuTransaction(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name="sender")
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, null=True, related_name="receiver")
    
    ubuntu = models.ForeignKey(Ubuntu, on_delete=models.CASCADE, null=True)

    amount = models.FloatField(default=0.0)
    transactionDate = models.DateTimeField(null=True, blank=True)

    is_sent = models.BooleanField(default=False)
    is_received = models.BooleanField(default=False)


    def __str__(self):
        return str(self.id)



class Record(models.Model):
    file = models.FileField()

    def __str__(self):
        return str(self.id)


class ChatMessage(models.Model):
    msgTypeChoices = (
        ('text', "Text Message"),
        ('record', "Voice Record"),
    )
    ubuntu = models.ForeignKey(Ubuntu, on_delete=models.CASCADE)
    msg_sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name="msg_sender")
    type = models.CharField(max_length=500)
    message = models.TextField(blank=True, null=True)
    record = models.ForeignKey(Record, blank=True, null=True, on_delete=models.SET_NULL, related_name="record")
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)