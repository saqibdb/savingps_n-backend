from requests.api import head
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from SavingPs.models import User, Bank, Category, Subcategory, Vendor, Transaction, Card, Pot, Tip, Key, PotTransaction, MonthlyDebit, MonthlyDebitCategory, MonthlyCredit
from SavingPs import Utility, Values
from random import randint
from twilio.rest import Client
from SavingPs_N import settings
from .serializers import TransactionSerializer, UserSerializer, VendorSerializer, CategorySerializer, MonthlyDebitSerializer, MonthlyDebitCategorySerializer, MonthlyCreditSerializer
from datetime import datetime, timedelta
import calendar
import requests
import re
from django_pandas.io import read_frame
from dateutil.relativedelta import relativedelta
from fbprophet import Prophet
import pandas as pd
import ahocorasick
import time
from django.db.models import Q

class Utility(object):

    def __init__(self, get_response):
        self.get_response = get_response


import json
def send_required_notifications():
    pass


def send_verification_to_phone(user):
    verificationCode = str(randint(1000, 9999))
    account_sid = settings.t_account_sid
    auth_token = settings.t_auth_token

    client = Client(account_sid, auth_token)

    verification = client.verify \
        .services(settings.t_service_id) \
        .verifications \
        .create(to=user.phone, channel='sms')

    user.verificationCode = verification.sid
    user.isPhoneVerified = False
    user.save()
    return verification


def varify_code(user, code):
    account_sid = settings.t_account_sid
    auth_token = settings.t_auth_token
    client = Client(account_sid, auth_token)

    verification_check = client.verify \
        .services(settings.t_service_id) \
        .verification_checks \
        .create(to=user.phone, code=code)

    if verification_check.status == "approved":
        user.isPhoneVerified = True
        user.save()
    return verification_check


def send_ubuntu_to_phone(phone, ownerName, ubuntuName):
    account_sid = settings.t_account_sid
    auth_token = settings.t_auth_token

    client = Client(account_sid, auth_token)

    message = client.messages \
                .create(
                     body= ownerName + "has invited you to join ubuntu circle (" +ubuntuName+ "). Join SavingPs today",
                     from_='+15017122661',
                     to=phone
                 )
    return message





def getTransactionCategories(user, request, is_last_month):
    allTransactionCategories = []

    today = datetime.utcnow()
    first_this_month = today.replace(day=1,hour=0, minute=0, second=0, microsecond=0)  # first date of current month


    last_previous_month = (first_this_month - timedelta(days=1)).replace(hour=23, minute=59, second=59, microsecond=999999)
    first_previous_month = last_previous_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)


    initial_date = first_this_month
    final_date = today

    if is_last_month == True:
        initial_date = first_previous_month
        final_date = last_previous_month


    transactions = Transaction.objects.filter(user=user, transactionType='debit',
                                              timestamp__lte=final_date, timestamp__gt=initial_date)


    print("Transactions are = "+str(len(transactions)))



    #transactions = Transaction.objects.filter(user=user, transactionType='debit')
    uncategories = Category.objects.filter(name='uncategorized')
    uncategory = list(uncategories)[0]

    for transaction in transactions:
        if transaction.category is None:
            transaction.category = uncategory
        amount = transaction.amount
        if amount < 0:
            amount = amount * -1

        foundTansactionCategory = next(
            (x for x in allTransactionCategories if x['id'] == transaction.category.id), None)
        if foundTansactionCategory is None:
            categories = Category.objects.filter(id=int(transaction.category.id))
            category = list(categories)[0]
            if not category.isDuplicateable:
                serializer = CategorySerializer(category, many=False, context={"request": request})

                allTransactionCategories.append({'id': transaction.category.id,
                                                 'amount': amount,
                                                 'category': serializer.data,
                                                 'total_transactions': 1})
        else:
            foundTansactionCategory['amount'] = foundTansactionCategory['amount'] + amount
            foundTansactionCategory['total_transactions'] = foundTansactionCategory['total_transactions'] + 1

        if transaction.vendor is not None and transaction.vendor.category is not None:
            if transaction.vendor.category.isDuplicateable:
                foundTansactionCategory = next(
                    (x for x in allTransactionCategories if x['id'] == transaction.vendor.category.id), None)
                if foundTansactionCategory is None:
                    categories = Category.objects.filter(id=int(transaction.vendor.category.id))
                    serializer = CategorySerializer(category, many=False, context={"request": request})

                    category = list(categories)[0]
                    allTransactionCategories.append({'id': transaction.vendor.category.id,
                                                     'amount': amount,
                                                     'category': serializer.data,
                                                     'total_transactions': 1})
                else:
                    foundTansactionCategory['amount'] = foundTansactionCategory['amount'] + amount
                    foundTansactionCategory['total_transactions'] = foundTansactionCategory['total_transactions'] + 1

    for transactionCategory in allTransactionCategories:
        transactionCategory['amount'] = float(format(transactionCategory['amount'], '.2f'))
    return allTransactionCategories



def getLastMonthTopCategories(user, request):
    allTransactionCategories = []

    today = datetime.utcnow()
    first_this_month = today.replace(day=1,hour=0, minute=0, second=0, microsecond=0)  # first date of current month

    last_previous_month = (first_this_month - timedelta(days=1)).replace(hour=23, minute=59, second=59, microsecond=999999)
    first_previous_month = last_previous_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)

    initial_date = first_previous_month
    final_date = last_previous_month

    transactions = Transaction.objects.filter(user=user, transactionType='debit',
                                              timestamp__lte=final_date, timestamp__gt=initial_date)

    uncategories = Category.objects.filter(name='uncategorized')
    uncategory = list(uncategories)[0]

    for transaction in transactions:
        if transaction.category is None:
            transaction.category = uncategory
        amount = transaction.amount
        if amount < 0:
            amount = amount * -1

        foundTansactionCategory = next(
            (x for x in allTransactionCategories if x['id'] == transaction.category.id), None)
        if foundTansactionCategory is None:
            categories = Category.objects.filter(id=int(transaction.category.id))
            category = list(categories)[0]
            if not category.isDuplicateable:
                serializer = CategorySerializer(category, many=False, context={"request": request})
                if transaction.category.id != uncategory.id:
                    allTransactionCategories.append({'id': transaction.category.id,
                                                     'amount': amount,
                                                     'category': serializer.data,
                                                     'total_transactions': 1})

        else:
            foundTansactionCategory['amount'] = foundTansactionCategory['amount'] + amount
            foundTansactionCategory['total_transactions'] = foundTansactionCategory['total_transactions'] + 1

        if transaction.vendor is not None and transaction.vendor.category is not None:
            if transaction.vendor.category.isDuplicateable:
                foundTansactionCategory = next(
                    (x for x in allTransactionCategories if x['id'] == transaction.vendor.category.id), None)
                if foundTansactionCategory is None:
                    categories = Category.objects.filter(id=int(transaction.vendor.category.id))
                    serializer = CategorySerializer(category, many=False, context={"request": request})

                    category = list(categories)[0]
                    if transaction.category.id != uncategory.id:
                        allTransactionCategories.append({'id': transaction.vendor.category.id,
                                                         'amount': amount,
                                                         'category': serializer.data,
                                                         'total_transactions': 1})
                else:
                    foundTansactionCategory['amount'] = foundTansactionCategory['amount'] + amount
                    foundTansactionCategory['total_transactions'] = foundTansactionCategory['total_transactions'] + 1

    for transactionCategory in allTransactionCategories:
        transactionCategory['amount'] = float(format(transactionCategory['amount'], '.2f'))
    return allTransactionCategories





def getHighestRetailerOfUser(user, is_last_month):
    today = datetime.utcnow()
    first_this_month = today.replace(day=1, hour=0, minute=0, second=0,
                                     microsecond=0)  # first date of current month

    last_previous_month = (first_this_month - timedelta(days=1)).replace(hour=23, minute=59, second=59,
                                                                                  microsecond=999999)
    first_previous_month = last_previous_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)

    initial_date = first_this_month
    final_date = today

    if is_last_month == True:
        initial_date = first_previous_month
        final_date = last_previous_month

    allTransactionsWithVendors = []

    transactions = Transaction.objects.filter(user=user, transactionType='debit', timestamp__lte=final_date, timestamp__gt=initial_date).exclude(vendor__isnull=True)

    for transaction in transactions:
        if transaction.vendor is not None:
            if transaction.vendor.isRetailer:
                amount = transaction.amount
                if amount < 0:
                    amount = amount * -1

                foundTansactionCategory = next(
                    (x for x in allTransactionsWithVendors if x['id'] == transaction.vendor.id), None)

                if foundTansactionCategory is None:
                    allTransactionsWithVendors.append({'id': transaction.vendor.id,
                                                       'amount': amount,
                                                       'vendor': transaction.vendor,
                                                       'total_transactions': 1})

                else:
                    foundTansactionCategory['amount'] = foundTansactionCategory['amount'] + amount
                    foundTansactionCategory['total_transactions'] = foundTansactionCategory[
                                                                        'total_transactions'] + 1

    allTransactionsWithVendors = sorted(allTransactionsWithVendors, key=lambda k: k['amount'], reverse=True)

    if len(allTransactionsWithVendors) == 0:
        return Response({"text": Values.noHighestRetailer_error()})

    highestRetailerDict = list(allTransactionsWithVendors)[0]

    serializer = VendorSerializer(highestRetailerDict['vendor'], many=False)
    return Response({"text": Values.highestRetailer_text(), "vendor": serializer.data,
                     'amount': highestRetailerDict['amount']})


def getMostFrequentShopOfUser(user, is_last_month):
    today = datetime.utcnow()
    first_this_month = today.replace(day=1, hour=0, minute=0, second=0,
                                     microsecond=0)  # first date of current month

    last_previous_month = (first_this_month - timedelta(days=1)).replace(hour=23, minute=59, second=59,
                                                                         microsecond=999999)
    first_previous_month = last_previous_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)

    initial_date = first_this_month
    final_date = today

    if is_last_month == True:
        initial_date = first_previous_month
        final_date = last_previous_month


    allTransactionsWithVendors = []

    transactions = Transaction.objects.filter(user=user, transactionType='debit', timestamp__lte=final_date, timestamp__gt=initial_date).exclude(vendor__isnull=True)

    for transaction in transactions:
        if transaction.vendor is not None:
            amount = transaction.amount
            if amount < 0:
                amount = amount * -1

            foundTansactionCategory = next(
                (x for x in allTransactionsWithVendors if x['id'] == transaction.vendor.id), None)

            if foundTansactionCategory is None:
                allTransactionsWithVendors.append({'id': transaction.vendor.id,
                                                   'amount': amount,
                                                   'vendor': transaction.vendor,
                                                   'total_transactions': 1})

            else:
                foundTansactionCategory['amount'] = foundTansactionCategory['amount'] + amount
                foundTansactionCategory['total_transactions'] = foundTansactionCategory[
                                                                    'total_transactions'] + 1

    allTransactionsWithVendors = sorted(allTransactionsWithVendors, key=lambda k: k['total_transactions'], reverse=True)

    if len(allTransactionsWithVendors) == 0:
        return Response({"text": Values.noMostFrequentShop_error()})

    highestRetailerDict = list(allTransactionsWithVendors)[0]

    serializer = VendorSerializer(highestRetailerDict['vendor'], many=False)
    return Response({"text": Values.mostFrequentShop_text(), "vendor": serializer.data,
                     'total_transactions': highestRetailerDict['total_transactions']})


def getHighestVendorsOfUser(user, is_last_month):
    today = datetime.utcnow()
    first_this_month = today.replace(day=1, hour=0, minute=0, second=0,
                                     microsecond=0)  # first date of current month

    last_previous_month = (first_this_month - timedelta(days=1)).replace(hour=23, minute=59, second=59,
                                                                         microsecond=999999)
    first_previous_month = last_previous_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)

    initial_date = first_this_month
    final_date = today

    if is_last_month == True:
        initial_date = first_previous_month
        final_date = last_previous_month

    allTransactionsWithVendors = []

    transactions = Transaction.objects.filter(user=user, transactionType='debit', timestamp__lte=final_date, timestamp__gt=initial_date).exclude(vendor__isnull=True)

    for transaction in transactions:
        if transaction.vendor is not None:
            amount = transaction.amount
            if amount < 0:
                amount = amount * -1

            foundTansactionCategory = next(
                (x for x in allTransactionsWithVendors if x['id'] == transaction.vendor.id), None)

            if foundTansactionCategory is None:
                allTransactionsWithVendors.append({'id': transaction.vendor.id,
                                                   'amount': amount,
                                                   'vendor': transaction.vendor,
                                                   'total_transactions': 1})

            else:
                foundTansactionCategory['amount'] = foundTansactionCategory['amount'] + amount
                foundTansactionCategory['total_transactions'] = foundTansactionCategory[
                                                                    'total_transactions'] + 1

    allTransactionsWithVendors = sorted(allTransactionsWithVendors, key=lambda k: k['amount'], reverse=True)

    if len(allTransactionsWithVendors) == 0:
        return Response({"text": Values.noHighestVendors_error()})
    top3 = allTransactionsWithVendors[:3]

    topVendors = []
    for transactionsWithVendor in top3:
        topVendors.append(transactionsWithVendor['vendor'])

    serializer = VendorSerializer(topVendors, many=True)
    return Response({"text": Values.highestVendors_text(), "vendors": serializer.data})


def getDailyAverageOfUser(user, is_last_month):
    today = datetime.utcnow()
    first_this_month = today.replace(day=1, hour=0, minute=0, second=0,
                                     microsecond=0)  # first date of current month

    last_previous_month = (first_this_month - timedelta(days=1)).replace(hour=23, minute=59, second=59,
                                                                         microsecond=999999)
    first_previous_month = last_previous_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)

    initial_date = first_this_month
    final_date = today

    if is_last_month == True:
        initial_date = first_previous_month
        final_date = last_previous_month


    amountTotal = 0
    transactions = Transaction.objects.filter(user=user, transactionType='debit', timestamp__lte=final_date, timestamp__gt=initial_date)
    for transaction in transactions:
        amount = transaction.amount
        if amount < 0:
            amount = amount * -1
        amountTotal = amountTotal + amount

    averageSpend = amountTotal / final_date.day

    return Response({"text": Values.dailyAverage_text(), "value": round(averageSpend, 2)})


def lastMonthSpendingUser(user):
    

    today = datetime.utcnow()
    first_this_month = today.replace(day=1, hour=0, minute=0, second=0,
                                     microsecond=0)  # first date of current month

    last_previous_month = (first_this_month - timedelta(days=1)).replace(hour=23, minute=59, second=59,
                                                                         microsecond=999999)
    first_previous_month = last_previous_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)




    
    thisMonthAmountTotal = 0
    thisMonthTransactions = Transaction.objects.filter(user=user, transactionType='debit',
                                                       timestamp__lte=today,
                                                       timestamp__gt=first_this_month)
    for transaction in thisMonthTransactions:
        amount = transaction.amount
        if amount < 0:
            amount = amount * -1
        thisMonthAmountTotal = thisMonthAmountTotal + amount
    last_day_previouse_month = end_date_of_a_month(last_previous_month).day
    if today.day < last_day_previouse_month:
        last_day_previouse_month = today.day
    #print("LAST MONTH ISSUE = "+str(end_date_of_a_month(last_previous_month)) +"__"+ str(last_day_previouse_month))

    lastMonthAmountTotal = 0
    lastMonthTransactions = Transaction.objects.filter(user=user, transactionType='debit',
                                                       timestamp__lte=last_previous_month.replace(day=last_day_previouse_month),
                                                       timestamp__gt=first_previous_month)
    
    for transaction in lastMonthTransactions:
        amount = transaction.amount
        if amount < 0:
            amount = amount * -1
        lastMonthAmountTotal = lastMonthAmountTotal + amount

    if thisMonthAmountTotal > lastMonthAmountTotal:
        return Response({"text": Values.lastMonthMore_text(), "value": round((thisMonthAmountTotal - lastMonthAmountTotal), 2)})
    else:
        return Response({"text": Values.lastMonthLess_text(), "value": round((lastMonthAmountTotal - thisMonthAmountTotal), 2)})



def end_date_of_a_month(date):
    start_date_of_this_month = date.replace(day=1)
    month = start_date_of_this_month.month
    year = start_date_of_this_month.year
    if month == 12:
        month = 1
        year += 1
    else:
        month += 1
    next_month_start_date = start_date_of_this_month.replace(month=month, year=year)

    this_month_end_date = next_month_start_date - timedelta(days=1)
    return this_month_end_date



def getPredictionTest(user):
    datesArr = []
    monthsArr = []
    #datesArr.append({"ds": get_past_date('today'), "y": 0.0})
    for i in range(1, 60):
        datesArr.append({"ds": get_past_date(str(i)+' days ago'), "y": 0.0})

    for i in range(1, 2):
        monthsArr.append({"month": calendar.month_name[(i * -1)], "spent": 0.0})

    transactions = Transaction.objects.filter(user=user, transactionType='debit',
                                              timestamp__lte=datesArr[0]["ds"], timestamp__gt=datesArr[-1]["ds"])




    for transaction in transactions:
        for date in datesArr:
            if date["ds"].date() == transaction.timestamp.date():
                date["y"] = date["y"] + (transaction.amount * -1)
        for month in monthsArr:
            if month["month"] == calendar.month_name[transaction.timestamp.month]:
                month["spent"] = month["spent"] + (transaction.amount * -1)

    for date in datesArr:
        date["ds"] = date["ds"].strftime('%Y-%m-%d')


    df = pd.DataFrame(datesArr, columns=['ds', 'y'])
    m = Prophet()
    m.fit(df)
    daysLeft = calendar.monthrange(datetime.now().year, datetime.now().month)[1] - datetime.now().day
    daysInNextMonth = calendar.monthrange((datetime.now().year + ((datetime.now().month + 1) % 12)), ((datetime.now().month + 1) % 12))[1]

    future = m.make_future_dataframe(periods=(daysLeft + daysInNextMonth))
    future.tail()
    forecast = m.predict(future)
    forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail()
    data = {"days_left_this_month": daysLeft,
            "prediction_for_days_remaining_this_month": round(sum(forecast['yhat'][((daysLeft + daysInNextMonth) * -1):(daysInNextMonth * -1)]), 2),
            "days_in_next_month": daysInNextMonth,
            "prediction_next_month": round(sum(forecast['yhat'][(daysInNextMonth * -1):]), 2),
            "previous_months": monthsArr,
            "forecast_days": forecast['ds'],
            "forecast_YHAT": forecast['yhat'],
            "df": df}
    return data
    '''
    bs = read_frame(Transaction.objects.filter(
        user=user
    ), fieldnames=['timestamp', 'amount'])
    bs['amount'] = abs(bs['amount']).astype(float)
    bs['date'] = pd.to_datetime(bs['timestamp'])

    bs['date'] = bs['date'].dt.tz_convert(None)
    bs = bs.rename(columns={'date': 'ds', 'amount': 'y'})
    m = Prophet(yearly_seasonality=False, weekly_seasonality=False,interval_width=0.95).fit(bs)
    future = m.make_future_dataframe(1, freq = "M")
    fcst = m.predict(future)
    fcst[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail()

    return fcst
    '''

def get_past_date(str_days_ago):

    TODAY = datetime.utcnow()
    splitted = str_days_ago.split()
    if len(splitted) == 1 and splitted[0].lower() == 'today':
        return TODAY.replace(hour=23, minute=59, second=59, microsecond=999999)
    elif splitted[1].lower() in ['day', 'days', 'd']:
        print("HOLLLLLLLLWW")
        date = TODAY - relativedelta(days=int(splitted[0]))

        return date.replace(hour=0, minute=0, second=0, microsecond=0)
    else:
        return "Wrong Argument format"

def handleBanksResponse(accountsDictArr, user, auto):
    newBankTransactionsCount = 0

    for accountDictArr in accountsDictArr:
        accountId = accountDictArr.get('account_id')
        currency = accountDictArr.get('currency')
        accountNumber = accountDictArr.get('account_number').get('swift_bic') + accountDictArr.get(
            'account_number').get('number') if accountDictArr.get(
            'account_number').get('number') is not None else ""
        
        bankName = accountDictArr.get('provider').get('display_name')

        allFoundBanks = Bank.objects.filter(accountId=str(accountId), user=user)

        currentBank = None

        datesArr = []

        toDate = datetime.now() - timedelta(days=364)

        if allFoundBanks.count() == 0:
            newBank = Bank(user=user,
                           accountId=accountId,
                           currency=str(currency),
                           accountNumber=accountNumber,
                           bankName=bankName)
            newBank.save()
            currentBank = newBank
            #datesArr.append({"from": datetime.now() - timedelta(days=364), "to": datetime.now()})
            #datesArr.append({"from": datetime.now() - timedelta(days=2*364), "to": datetime.now() - timedelta(days=364)})
            #datesArr.append({"from": datetime.now() - timedelta(days=3*364), "to": datetime.now() - timedelta(days=2*364)})
            datesArr.append({"from": datetime.now() - timedelta(days=60), "to": datetime.now()})

        else:
            currentBank = list(allFoundBanks)[0]
            toDate = currentBank.lastUpdationDate
            currentBank.lastUpdationDate = datetime.now().isoformat()
            currentBank.save()
            datesArr.append({"from": toDate, "to": datetime.now()})

        for date in datesArr:
            url = "https://api.truelayer.com/data/v1/accounts/" + currentBank.accountId + "/transactions?from=" + \
                  date["from"].isoformat() + "&to=" + date["to"].isoformat()
            url = url.replace('+', '%2B')
            print("URL______" + url)

            
            payload = {}
            headers = {
                'Authorization': 'Bearer ' + user.accessToken
            }

            responseTransactions = requests.request("GET", url, headers=headers, data=payload)
            responseTransactionsDict = responseTransactions.json()
            if "results" in responseTransactionsDict:
                transactionsDictArr = responseTransactionsDict.get('results')
                transactionsDictArrFiltered = transactionsDictArr
                #transactionsDictArrFiltered = list(filter(lambda x: x.get('transaction_type') == 'DEBIT', transactionsDictArr))
                
                newBankTransactionsCount = newBankTransactionsCount + handleTransactionsResponse(
                    transactionsDictArrFiltered, user, currentBank, None, auto)
                
    
    return newBankTransactionsCount


def handleCardsResponse(accountsDictArr, user, auto):
    newCardsTransactionsCount = 0
    for accountDictArr in accountsDictArr:
        accountId = accountDictArr.get('account_id')
        currency = accountDictArr.get('currency')
        cardNumber = accountDictArr.get('partial_card_number')
        cardName = accountDictArr.get('name_on_card')
        cardProvider = accountDictArr.get('display_name')
        cardNetwork = accountDictArr.get('card_network')
        cardType = accountDictArr.get('card_type')

        allFoundCards = Card.objects.filter(accountId=str(accountId), user=user)

        currentCard = None

        datesArr = []

        toDate = datetime.now() - timedelta(days=364)

        if allFoundCards.count() == 0:
            newCard = Card(user=user,
                           accountId=accountId,
                           currency=str(currency),
                           cardNumber=cardNumber,
                           cardName=cardName,
                           cardProvider=cardProvider,
                           cardNetwork=cardNetwork,
                           cardType=cardType)
            newCard.save()
            currentCard = newCard
            #datesArr.append({"from": datetime.now() - timedelta(days=364), "to": datetime.now()})
            #datesArr.append({"from": datetime.now() - timedelta(days=2*364), "to": datetime.now() - timedelta(days=364)})
            #datesArr.append({"from": datetime.now() - timedelta(days=3*364), "to": datetime.now() - timedelta(days=2*364)})
            datesArr.append({"from": datetime.now() - timedelta(days=60), "to": datetime.now()})

        else:
            currentCard = list(allFoundCards)[0]
            toDate = currentCard.lastUpdationDate
            currentCard.lastUpdationDate = datetime.now().isoformat()
            currentCard.save()
            datesArr.append({"from": toDate, "to": datetime.now()})

        for date in datesArr:
            url = "https://api.truelayer.com/data/v1/cards/" + currentCard.accountId + "/transactions?from=" + \
                  date["from"].isoformat() + "&to=" + date["to"].isoformat()
            url = url.replace('+', '%2B')
            payload = {}
            headers = {
                'Authorization': 'Bearer ' + user.accessToken
            }

            responseTransactions = requests.request("GET", url, headers=headers, data=payload)
            responseTransactionsDict = responseTransactions.json()
            if "results" in responseTransactionsDict:
                transactionsDictArr = responseTransactionsDict.get('results')
                transactionsDictArrFiltered = transactionsDictArr

                newCardsTransactionsCount = newCardsTransactionsCount + handleTransactionsResponse(
                    transactionsDictArrFiltered, user, None, currentCard, auto)
    return newCardsTransactionsCount


def handleTransactionsResponse(transactionsDictArr, user, currentBank, currentCard, auto):
    print("TOTAL TRANSACTIONS = "+ str(len(transactionsDictArr)))
    
    if len(transactionsDictArr) == 0:
        return 0
    user.isBankConnected = True
    user.save()
    
    newTransactionsCount = 0
    unCategorizedCategories = Category.objects.filter(name__iexact='uncategorized')
    unCategorizedCategory = list(unCategorizedCategories)[0]

    for transactionDict in transactionsDictArr:
        
        transactionId = transactionDict.get('transaction_id')
        if str(transactionId) == "eb9a1e2dc78b2f3dbf4305588dad4cfd" or str(transactionId) == "d380628cfb06563ce7b5b429d612a148":
            print("TRANSACTION = "+str(transactionDict))
        allTransactions = Transaction.objects.filter(transactionId=str(transactionId), user=user)
        
        if allTransactions.count() == 0:
            
            if transactionDict.get('transaction_type') == 'CREDIT':
                
                handleCredit(transactionDict, user, currentBank, currentCard)
            else:
                
                category = None
                vendor = None
                subcategory = None
                
                transactionClassifications = transactionDict.get('transaction_classification')
                if len(transactionClassifications) > 0:
                    for transactionClassification in reversed(transactionClassifications):
                        allFoundSubCats = Subcategory.objects.filter(
                            name__iexact=str(transactionClassification).lower())
                        if allFoundSubCats.count() > 0:
                            subCat = list(allFoundSubCats)[0]
                            subcategory = subCat
                            category = subCat.category
                            break

                merchantName = None
                providerMerchantName = None
                if "merchant_name" in transactionDict:
                    merchantName = transactionDict.get('merchant_name')
                elif "meta" in transactionDict:
                    metaDict = transactionDict.get('meta')
                    if "meta" in metaDict:
                        if merchantName == None:
                            merchantName = metaDict.get('provider_merchant_name')
                        providerMerchantName = metaDict.get('provider_merchant_name')
                
                if merchantName != None:
                    allFoundVendors = Vendor.objects.filter(name__iexact=str(merchantName).lower()).order_by('id')
                    if allFoundVendors.count() > 0:
                        vendor = list(allFoundVendors)[0]
                        if category == None:
                            category = vendor.category
                    else:
                        if len(merchantName) > 0:
                            #newVendor = Vendor(name=merchantName.lower(), category=category)
                            newVendor = Vendor(name=merchantName.lower())

                            newVendor.save()
                            if category == None:
                                category = newVendor.category
                            
                            vendor = newVendor

                
                """
                if merchantName == None:
                    allVendors = Vendor.objects.all()
                    with concurrent.futures.ThreadPoolExecutor() as executor:
                        results = [executor.submit(searchVendorInDescription ,vendorObj, transactionDict.get('description'), vendor, category, merchantName) for vendorObj in allVendors]
    
                        for result in results:
                            print(result)
    
                """
                
                if len(transactionsDictArr) < 4000:
                    if merchantName == None or vendor == None:
                        indexFound = 0
                        for end_ind, (vendorObj, found) in list(auto.iter_long(str(transactionDict.get('description')).lower())):
                            if len(found) > 3:
                                vendor = vendorObj
                                merchantName = found
                                if category == None:
                                    category = vendor.category
                                break
                            else:
                                start_index = end_ind - len(found) + 1
                                pre = False
                                post = False

                                if end_ind+1 >= len(str(transactionDict.get('description')).lower()):
                                    post = True
                                else:
                                    nextChar = str(transactionDict.get('description')).lower()[end_ind + 1]
                                    if re.search(r'[^a-zA-Z]', nextChar):
                                        post = True

                                if start_index == 0:
                                    pre = True
                                else:
                                    prevChar = str(transactionDict.get('description')).lower()[start_index - 1]
                                    if re.search(r'[^a-zA-Z]', prevChar):
                                        pre = True
                                if post == True and pre == True:
                                    vendor = vendorObj
                                    merchantName = found
                                    if category == None:
                                        category = vendor.category
                                    break
                else:
                    print("The count of transactions is " + str(len(transactionsDictArr)))

                
                timeStr = transactionDict.get('timestamp').split('.', 1)[0].replace('Z', '')

                if isTimeFormat(transactionDict.get('timestamp')) == True:
                    timestampObj = datetime.strptime(timeStr,
                                                 '%Y-%m-%dT%H:%M:%SS')
                else:
                    timestampObj = datetime.strptime(timeStr,
                                                 '%Y-%m-%dT%H:%M:%S')

                
                if timestampObj == None:
                    timestampObj = datetime.now()
                    print(transactionDict.get('timestamp'))
                if category == None:
                    category = unCategorizedCategory
                newTransaction = Transaction(category=category,
                                             subcategory=subcategory,
                                             vendor=vendor,
                                             user=user,
                                             bank=currentBank,
                                             card=currentCard,
                                             timestamp=timestampObj,
                                             description=transactionDict.get('description'),
                                             transactionType=transactionDict.get(
                                                 'transaction_type').lower(),
                                             merchantName=merchantName,
                                             amount=transactionDict.get('amount'),
                                             currency=transactionDict.get('currency'),
                                             transactionId=transactionId,
                                             metaMerchantName=providerMerchantName,
                                             transactionClassificationString=str(
                                                 transactionDict.get('transaction_classification')))

                newTransaction.save()
                newTransactionsCount = newTransactionsCount + 1
    return newTransactionsCount

def handleCredit(transactionDict, user, currentBank, currentCard):
    timeStr = transactionDict.get('timestamp').split('.', 1)[0].replace('Z', '')
    if isTimeFormat(transactionDict.get('timestamp')) == True:
        timestampObj = datetime.strptime(timeStr,
                                                 '%Y-%m-%dT%H:%M:%SS')
    else:
        timestampObj = datetime.strptime(timeStr,
                                                 '%Y-%m-%dT%H:%M:%S')
    
    if timestampObj == None:
        timestampObj = datetime.now()
        print(transactionDict.get('timestamp'))
    providerMerchantName = None
    if "meta" in transactionDict:
        metaDict = transactionDict.get('meta')
        if "meta" in metaDict:
            providerMerchantName = metaDict.get('provider_merchant_name')
    newTransaction = Transaction(category=None,
                                 subcategory=None,
                                 vendor=None,
                                 user=user,
                                 bank=currentBank,
                                 card=currentCard,
                                 timestamp=timestampObj,
                                 description=transactionDict.get('description'),
                                 transactionType=transactionDict.get(
                                     'transaction_type').lower(),
                                 merchantName=None,
                                 amount=transactionDict.get('amount'),
                                 currency=transactionDict.get('currency'),
                                 transactionId=transactionDict.get('transaction_id'),
                                 metaMerchantName=providerMerchantName,
                                 transactionClassificationString=str(
                                     transactionDict.get('transaction_classification')))
    newTransaction.save()


def refreshToken(user):
    if user.accessTokenExpiration is not None:
        if datetime.now() > user.accessTokenExpiration:
            url = "https://auth.truelayer.com/connect/token"

            payload = 'grant_type=refresh_token&client_id=' + settings.tl_client_id + '&client_secret=' + settings.tl_client_secret + '&refresh_token=' + user.refreshToken
            headers = {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
            response = requests.request("POST", url, headers=headers, data=payload)
            responseDict = response.json()

            if "access_token" in responseDict:
                user.accessToken = responseDict.get('access_token')
            else:
                return Response({"Response": response.json(), "status": "failed!"})

            if "token_type" in responseDict:
                user.tokenType = str(responseDict.get('token_type'))
            if "refresh_token" in responseDict:
                user.refreshToken = str(responseDict.get('refresh_token'))
            if "expires_in" in responseDict:
                user.accessTokenExpiration = datetime.now() + timedelta(seconds=responseDict.get('expires_in'))
            user.save()

def fetchBalance(user):
    refreshToken(user)
    url = ""
    allFoundBanks = Bank.objects.filter(user=user)
    
    if allFoundBanks.count() > 0:
        currentBank = list(allFoundBanks)[0]
        url = "https://api.truelayer.com/data/v1/accounts/" + currentBank.accountId + "/balance"
    else:
        allFoundCards = Card.objects.filter(user=user)
        if allFoundCards.count() == 0:
            return None
        currentCard = list(allFoundCards)[0]
        url = "https://api.truelayer.com/data/v1/cards/" + currentCard.accountId + "/balance"
    
    payload = {}
    headers = {
        'Authorization': 'Bearer ' + user.accessToken
    }

    responseTransactions = requests.request("GET", url, headers=headers, data=payload)
    responseTransactionsDict = responseTransactions.json()
    print("RESPONSE = "+ str(responseTransactionsDict))
    if "results" in responseTransactionsDict:
        return responseTransactionsDict.get("results")[0].get("available")       
    return None


def isTimeFormat(input):
    try:
        time.strptime(input, '%Y-%m-%dT%H:%M:%SS')
        return True
    except ValueError:
        return False



def getCategoriesForNoBank(isTopCategories, request):
    allTransactionCategories = []
    categories = Category.objects.all()
    if isTopCategories == True:
        categories = Category.objects.all()[:3]
    for category in categories:
        serializer = CategorySerializer(category, many=False, context={"request": request})
        allTransactionCategories.append({'id': category.id,
                                        'amount': 0.0,
                                        'category': serializer.data,
                                        'total_transactions': 0})
    for transactionCategory in allTransactionCategories:
        transactionCategory['amount'] = float(format(transactionCategory['amount'], '.2f'))
    return allTransactionCategories


def calculatePotSavings(user):
    allFoundPots = Pot.objects.filter(user=user,isCompleted=False ).order_by('potStartingDate')

    globalTransactionNeededMondays = []
    if allFoundPots.count() > 0:
        pot = list(allFoundPots)[0]
    #for pot in allFoundPots:
        d1 = pot.potStartingDate.replace(hour=0, minute=0, second=0, microsecond=0)   
        d2 = datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0)
        allMondays = find_days(d1, d2, 0)
        transactionNeededMondays = []

        for monday in allMondays:
            allPotsTransactions = PotTransaction.objects.filter(user=user,pot=pot,transactionDate=monday)
            if len(allPotsTransactions) == 0:
                transactionNeededMondays.append(monday)
                if monday not in globalTransactionNeededMondays:
                    globalTransactionNeededMondays.append(monday)

        query = Q(name='eating out')
        query.add(Q(name='retail & homeware'), Q.OR)
        query.add(Q(name='entertainment'), Q.OR)
        query.add(Q(name='health & beauty'), Q.OR)


        categories = Category.objects.filter(query)
        print("categories count "+str(len(categories)))
        for date in transactionNeededMondays:
            week_ago = date - timedelta(days=7) - timedelta(seconds=1)
            sec_ago = date - timedelta(seconds=1)
            amount = 0

            queryTransaction = Q()
            for category in categories:
                queryTransaction.add(Q(category=category), Q.OR)
            
            queryTransaction.add(Q(user=user), Q.AND)
            queryTransaction.add(Q(transactionType='debit'), Q.AND)
            queryTransaction.add(Q(timestamp__lte=sec_ago), Q.AND)
            queryTransaction.add(Q(timestamp__gt=week_ago), Q.AND)

            transactions = Transaction.objects.filter(queryTransaction)
            
            for transaction in transactions:
                amount = amount + (transaction.amount*-1)
            amount = amount / 10.0

            potSaving = 0.0
            extraAmount = 0.0
            allFoundPotTransactions = PotTransaction.objects.filter(pot=pot)
            for potTransaction in allFoundPotTransactions:
                potSaving = potSaving + potTransaction.amount
            if (potSaving + amount) > pot.targetAmount:
                extraAmount = (potSaving + amount) - pot.targetAmount
                amount = pot.targetAmount - potSaving
            if pot.isCompleted == False:
                newPotTransaction = PotTransaction(user=user, pot=pot, amount=amount,transactionDate=date)
                newPotTransaction.save()
                if (potSaving + amount) == pot.targetAmount:
                    pot.isCompleted = True
                    pot.save()
                    if extraAmount > 0.0:
                        if len(allFoundPots) > 1:
                            pot = list(allFoundPots)[1]
                            if pot.potStartingDate < date:
                                print("pot.potStartingDate count "+str(pot.potStartingDate))
                                if extraAmount > pot.targetAmount:
                                    extraAmount = extraAmount - pot.targetAmount
                                newPotTransaction = PotTransaction(user=user, pot=pot, amount=extraAmount,transactionDate=date)
                                newPotTransaction.save()
                                if extraAmount == pot.targetAmount:
                                    pot.isCompleted = True
                                pot.potStartingDate = date
                                pot.save()
    return globalTransactionNeededMondays


def getSavingsThisMonth(user):
    allTransactionCategories = []

    today = datetime.utcnow()
    first_this_month = today.replace(day=1,hour=0, minute=0, second=0, microsecond=0)  # first date of current month


    last_previous_month = (first_this_month - timedelta(days=1)).replace(hour=23, minute=59, second=59, microsecond=999999)
    first_previous_month = last_previous_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)


    initial_date = first_this_month
    final_date = today

    totalSavings = 0.0
    allFoundPotTransactions = PotTransaction.objects.filter(user=user,transactionDate__lte=final_date, transactionDate__gt=initial_date)
    for potTransaction in allFoundPotTransactions:
        totalSavings = totalSavings + potTransaction.amount
    return totalSavings





def getUserDebitsCredits(user, request):

    previousMonthSpentObj = previousMonthSpent(user)

    allMonthlyDebits = MonthlyDebit.objects.filter(user=user)
    if allMonthlyDebits.count() == 0:
        return Response({"status": "No debit found!", "previos_month_spent" : previousMonthSpentObj})
    else:
        monthlyDebit = list(allMonthlyDebits)[0]
        serializerMonthlyDebit = MonthlyDebitSerializer(monthlyDebit, many=False)

        debitResponse = serializerMonthlyDebit.data

        
        allDebitsCategorys = MonthlyDebitCategory.objects.filter(monthlyDebit=monthlyDebit)        
        serializerMonthlyDebitCategory = MonthlyDebitCategorySerializer(list(allDebitsCategorys), many=True, context={"request": request})

        
        debitCategories = serializerMonthlyDebitCategory.data
        for debitCategory in debitCategories:
            debitCategory['current_spent'] = categorySpent(user, debitCategory['category']['id'])


        debitResponse['debit_categories'] = serializerMonthlyDebitCategory.data
        
        allMonthlyCredits = MonthlyCredit.objects.filter(user=user)
        serializerMonthlyCredit = MonthlyCreditSerializer(list(allMonthlyCredits), many=True)
    
    
        return Response({"debit": debitResponse, "credits" : serializerMonthlyCredit.data, "previos_month_spent" : previousMonthSpentObj})



def previousMonthSpent(user):
    today = datetime.utcnow()
    first_this_month = today.replace(day=1, hour=0, minute=0, second=0,
                                     microsecond=0)  # first date of current month

    last_previous_month = (first_this_month - timedelta(days=1)).replace(hour=23, minute=59, second=59,
                                                                         microsecond=999999)
    first_previous_month = last_previous_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)



    lastMonthAmountTotal = 0
    lastMonthTransactions = Transaction.objects.filter(user=user, transactionType='debit',
                                                       timestamp__lte=last_previous_month,
                                                       timestamp__gt=first_previous_month)
    
    for transaction in lastMonthTransactions:
        amount = transaction.amount
        if amount < 0:
            amount = amount * -1
        lastMonthAmountTotal = lastMonthAmountTotal + amount

    
    return lastMonthAmountTotal


def categorySpent(user, categoryId):
    today = datetime.utcnow()
    first_this_month = today.replace(day=1, hour=0, minute=0, second=0,
                                     microsecond=0)  # first date of current month

    lastMonthAmountTotal = 0
    lastMonthTransactions = Transaction.objects.filter(user=user, transactionType='debit',
                                                        category__id=categoryId,
                                                       timestamp__lte=today,
                                                       timestamp__gt=first_this_month)
    
    for transaction in lastMonthTransactions:
        amount = transaction.amount
        if amount < 0:
            amount = amount * -1
        lastMonthAmountTotal = lastMonthAmountTotal + amount

    
    return lastMonthAmountTotal



def helper(d, i, inc):
    while d.weekday() != i:
        d += timedelta(days=inc)
    return d.replace(hour=0, minute=0, second=0, microsecond=0)


def find_days(st, end, d1):
    if st >= end:
        return []
    else:
        days = (st + timedelta(days=i) for i in range((end - st).days + 1))
        l = [d for d in days if d.weekday() in [d1] ]
        return list(l)