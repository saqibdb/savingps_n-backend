# Generated by Django 3.1.2 on 2021-04-24 23:50

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('SavingPs', '0032_remove_pottransaction_potname'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pot',
            name='isDefault',
        ),
        migrations.RemoveField(
            model_name='pot',
            name='totalSavingAmount',
        ),
        migrations.RemoveField(
            model_name='pot',
            name='weeklyInstallmentAmount',
        ),
    ]
