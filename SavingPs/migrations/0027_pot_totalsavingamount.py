# Generated by Django 3.1.2 on 2021-02-09 05:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('SavingPs', '0026_auto_20210204_1014'),
    ]

    operations = [
        migrations.AddField(
            model_name='pot',
            name='totalSavingAmount',
            field=models.FloatField(default=0.0),
        ),
    ]
