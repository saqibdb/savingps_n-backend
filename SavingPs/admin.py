from django.contrib import admin
from SavingPs.models import User, Bank, Category, Subcategory, Vendor, Transaction, Card, Pot, Constant, Key, Tip, PotTransaction, MonthlyDebit, MonthlyDebitCategory, MonthlyCredit

from SavingPs.models import Ubuntu, UbuntuPhoneNumberInvite, UbuntuInvite, UbuntuMember, UbuntuShiftRequest, UbuntuTransaction, ChatMessage



from import_export.admin import ImportExportMixin, ExportActionModelAdmin

# Register your models here.

class UserAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'firstName',
        'email',
        'phone',
        'lastLoginDate',
        'authToken'
    )
    search_fields = ('firstName', 'email', 'phone')
admin.site.register(User, UserAdmin)




class BankAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'user',
        'bankName',
        'lastUpdationDate'
    )
    search_fields = ('user', 'bankName', 'accountNumber')
admin.site.register(Bank, BankAdmin)

class CardAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'user',
        'cardProvider',
        'lastUpdationDate'
    )
    search_fields = ('user', 'cardProvider', 'cardNumber')
admin.site.register(Card, CardAdmin)


class CategoryAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'name'
    )
    search_fields = ('id', 'name')
admin.site.register(Category, CategoryAdmin)


class SubcategoryAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'name'
    )
    search_fields = ('id', 'name')
admin.site.register(Subcategory, SubcategoryAdmin)



class VendorAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'name',
        'category'
    )
    list_per_page = 50
    search_fields = ('id', 'name', 'category__name')
admin.site.register(Vendor, VendorAdmin)



class VendorsWithoutCategory(Vendor):
    class Meta:
        proxy = True

class MyVendorAdmin(VendorAdmin):
    def get_queryset(self, request):
        return self.model.objects.filter(category= None)


admin.site.register(VendorsWithoutCategory, MyVendorAdmin)





class TransactionAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'category',
        'vendor',
        'subcategory',
        'description',
        'user',
        'bank',
        'amount',
        'transactionType',
        'transactionId',
        'timestamp',
        'creationDate'
    )
    search_fields = ('amount', 'id', 'transactionId', 'vendor__name',
                     'category__name', 'subcategory__name', 'user__phone',
                     'bank__bankName', 'transactionType')
admin.site.register(Transaction, TransactionAdmin)


class PotAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'user',
        'potName',
        'targetAmount',
        'potStartingDate',
        'isCompleted'
    )
    search_fields = ('potName', 'targetAmount', 'user__name')
admin.site.register(Pot, PotAdmin)


class ConstantAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'key',
        'text',
        'updationDate'
    )
    search_fields = ('key', 'text')
admin.site.register(Constant, ConstantAdmin)

class KeyAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'key',
        'text',
        'updationDate'
    )
    search_fields = ('key', 'text')
admin.site.register(Key, KeyAdmin)


class TipAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'text',
        'updationDate'
    )
    search_fields = ('id', 'text')
admin.site.register(Tip, TipAdmin)


class PotTransactionAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'amount',
        'transactionDate',
        'user',
        'pot'
    )
    search_fields = ('id', 'amount', 'user__name', 'pot__potName')
admin.site.register(PotTransaction, PotTransactionAdmin)


class MonthlyDebitAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'user',
        'income',
        'currentDate',
        'creationDate'
    )
    search_fields = ('id', 'income', 'user__name')
admin.site.register(MonthlyDebit, MonthlyDebitAdmin)


class MonthlyDebitCategoryAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'category',
        'monthlyDebit',
        'targetAmount',
        'creationDate'
    )
    search_fields = ('id', 'monthlyDebit__id', 'category__name')
admin.site.register(MonthlyDebitCategory, MonthlyDebitCategoryAdmin)


class MonthlyCreditAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'user',
        'creditName',
        'amountLeft',
        'currentDate',
        'creationDate'
    )
    search_fields = ('id', 'creditName', 'user__name')
admin.site.register(MonthlyCredit, MonthlyCreditAdmin)


class UbuntuAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'creator',
        'savingGoal',
        'monthlyContribution',
        'numberOfParticipants',
        'isStarted',
        'date',
        'creationDate'
    )
    search_fields = ('id', 'savingGoal', 'creator__name')
admin.site.register(Ubuntu, UbuntuAdmin)


class UbuntuPhoneNumberInviteAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'phoneNumber',
        'ubuntu',
        'status',
        'creationDate'
    )
    search_fields = ('id', 'status', 'phoneNumber')
admin.site.register(UbuntuPhoneNumberInvite, UbuntuPhoneNumberInviteAdmin)


class UbuntuInviteAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'user',
        'ubuntu',
        'status',
        'creationDate'
    )
    search_fields = ('id', 'status', 'user__name')
admin.site.register(UbuntuInvite, UbuntuInviteAdmin)


class UbuntuMemberAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'user',
        'ubuntu',
        'index'
    )
    search_fields = ('id', 'status', 'user__name')
admin.site.register(UbuntuMember, UbuntuMemberAdmin)


class UbuntuShiftRequestAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'sender',
        'receiver',
        'status',
        'sent'
    )
    search_fields = ('id', 'status')
admin.site.register(UbuntuShiftRequest, UbuntuShiftRequestAdmin)


class UbuntuTransactionAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'sender',
        'receiver',
        'ubuntu',
        'amount',
        'transactionDate',
        'is_sent',
        'is_received'
    )
    search_fields = ('id',)
admin.site.register(UbuntuTransaction, UbuntuTransactionAdmin)



class ChatMessageAdmin(ImportExportMixin, ExportActionModelAdmin):
    list_display = (
        'id',
        'ubuntu',
        'msg_sender',
        'type',
        'message',
        'record',
        'timestamp',
    )
    search_fields = ('id',)
admin.site.register(ChatMessage, ChatMessageAdmin)