from django.shortcuts import render
from django.core.files.base import ContentFile

# Create your views here.
import base64
import django
from rest_framework import status
from rest_framework.renderers import JSONRenderer

from rest_framework.response import Response
from rest_framework.decorators import api_view

from SavingPs.aes_encrypt import SQBCipher
from SavingPs.models import ChatMessage, UbuntuShiftRequest, UbuntuTransaction, User, Bank, Category, Subcategory, Vendor, Transaction, Card, Pot, Tip, Key, PotTransaction, MonthlyDebit, MonthlyDebitCategory, MonthlyCredit
from SavingPs.models import Ubuntu, UbuntuMember, UbuntuPhoneNumberInvite, UbuntuInvite, Record
import uuid
from django.db.models import Q

import json, os
from SavingPs import Utility, Values
from datetime import datetime, timedelta
import datetime
import requests
from SavingPs_N import settings
from .serializers import TransactionSerializer, UbuntuInviteSerializer, UbuntuShiftRequestSerializer, UserSerializer, VendorSerializer, CategorySerializer, PotSerializer, PotTransactionSerializer


from .serializers import UbuntuSerializer, UserInviteSerializer, ChatMessageSerializer

import concurrent.futures

from random import randint, choice
from django.utils import timezone
import ahocorasick
import time
from django.db.models import Count

# Create your views here.
@api_view(['POST', ])
def signUp(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            email = ""
            if "email" in body:
                email = str(body['email'])

            firstName = str(body['first_name'])
            surname = str(body['sur_name'])
            phone = str(body['phone'])
            postCode = None
            if 'post_code' in body:
                postCode = str(body['post_code'])
            address = None
            if 'address' in body:
                address = str(body['address'])

            dob = None
            if 'dob' in body:
                dateStr = str(body['dob'])
                format = '%d-%m-%Y'
                dob = datetime.datetime.strptime(dateStr, format).date()


            allFoundUsers = User.objects.filter(phone=str(phone))
            if allFoundUsers.count() > 0:
                return Response({"status": Values.anotherUser_error()})

            allFoundUsers = User.objects.filter(email=str(email))
            if allFoundUsers.count() > 0:
                return Response({"status": Values.anotherUser_error()})

            newAuthToken = uuid.uuid4()

            new = User(email=str(email),
                       postCode=postCode,
                       authToken=str(newAuthToken),
                       firstName=firstName,
                       surname=surname,
                       phone=phone,
                       address=address,
                       dob=dob)
            new.save()
            ##### verificationResponse = Utility.send_verification_to_phone(new)



            phoneNumberInvitations = UbuntuPhoneNumberInvite.objects.filter(phoneNumber=phone)
            for invitation in phoneNumberInvitations:
                real_invitation = UbuntuInvite(
                    user = new,
                    ubuntu = invitation.ubuntu,
                    creationDate = invitation.creationDate,
                    updationDate = invitation.updationDate,
                )
                real_invitation.save()
            
            phoneNumberInvitations.delete()




            serializer = UserSerializer(new, many=False, context={"request": request})

            return Response({"User": serializer.data, "status": Values.newUser_success()})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def verifyCode(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            verificationCode = str(body['code'])
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            ##### verificationResponse = Utility.varify_code(user, verificationCode)
            verificationResponse = "approved"
            ##### if verificationResponse.status == 'approved':
            if verificationResponse == 'approved':
                serializer = UserSerializer(user, many=False, context={"request": request})

                ##### return Response({"status": verificationResponse.status, "User": serializer.data})
                return Response({"status": verificationResponse, "User": serializer.data})
            else:
                return Response({"status": Values.wrongCode_error()})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def setPin(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            pin = str(body['pin'])
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUserPhone_error()})
            user = list(allFoundUsers)[0]
            if user.isPhoneVerified == False:
                return Response({"status": Values.phoneVerified_error()})
            user.pin = pin
            user.save()
            serializer = UserSerializer(user, many=False, context={"request": request})

            isCodeNeeded = False
            if user.accessTokenExpiration is None:
                isCodeNeeded = True
            else:
                if timezone.now() > user.accessTokenExpiration:
                    isCodeNeeded = True
            return Response({"status": Values.pin_success(), "User": serializer.data, "is_code_needed": isCodeNeeded})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def logIn(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            phone = str(body['phone']).lower()
            pin = body['pin']
            allFoundUsers = User.objects.filter(phone=str(phone))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUserPhone_error()})
            user = list(allFoundUsers)[0]
            if user.pin != str(pin):
                return Response({"status": Values.wrongPin_error()})

            if user.accessTokenExpiration is not None:
                if timezone.now() > user.accessTokenExpiration:
                    url = "https://auth.truelayer.com/connect/token"

                    payload = 'grant_type=refresh_token&client_id=' + settings.tl_client_id + '&client_secret=' + settings.tl_client_secret + '&refresh_token=' + user.refreshToken
                    headers = {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                    response = requests.request("POST", url, headers=headers, data=payload)
                    responseDict = response.json()

                    if "access_token" in responseDict:
                        user.accessToken = responseDict.get('access_token')
                    else:
                        if responseDict.get('error') == 'invalid_grant':
                            user.accessTokenExpiration = None
                            user.isBankConnected = False
                            user.save()
                        else:
                            return Response({"Response": response.json(), "status": "failed!"})

                    if "token_type" in responseDict:
                        user.tokenType = str(responseDict.get('token_type'))
                    if "refresh_token" in responseDict:
                        user.refreshToken = str(responseDict.get('refresh_token'))
                    if "expires_in" in responseDict:
                        user.accessTokenExpiration = timezone.now() + timedelta(seconds=responseDict.get('expires_in'))
                    user.save()
            isCodeNeeded = False
            if user.accessTokenExpiration is None:
                isCodeNeeded = True
            else:
                if timezone.now() > user.accessTokenExpiration:
                    isCodeNeeded = True
            serializer = UserSerializer(user, many=False, context={"request": request})

            return Response({"User": serializer.data, "status": Values.userFound_success(), "is_code_needed": isCodeNeeded})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def forgetPin(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            phone = str(body['phone']).lower()
            allFoundUsers = User.objects.filter(phone=str(phone))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUserPhone_error()})
            user = list(allFoundUsers)[0]
            verificationResponse = Utility.send_verification_to_phone(user)
            serializer = UserSerializer(user, many=False, context={"request": request})

            return Response({"User": serializer.data, "status": Values.forgetPin_success()})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getTransactions(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            newBanksTransactions = 0
            newCardsTransactions = 0
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            if user.isBankConnected == False and ('code' in body) == False:
                if user.refreshToken == None:
                    allTransactionCategories = Utility.getCategoriesForNoBank(True, request)
                else:
                    allTransactionCategories = Utility.getLastMonthTopCategories(user, request)
                return Response({"categories": list(allTransactionCategories), "status": "success!",
                                "new_banks_transactions": 0,
                                "new_cards_transactions": 0})


            is_last_month = False
            if 'is_last_month' in body:
                is_last_month = body['is_last_month']

            if 'code' in body:
                code = str(body['code'])
                url = "https://auth.truelayer.com/connect/token"

                payload = 'grant_type=authorization_code&client_id=' + settings.tl_client_id + '&client_secret=' + settings.tl_client_secret + '&redirect_uri=' + settings.tl_redirect_url + '&code=' + code
                headers = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
                response = requests.request("POST", url, headers=headers, data=payload)
                responseDict = response.json()

                if "access_token" in responseDict:
                    user.accessToken = responseDict.get('access_token')
                else:
                    #print(str(response.json()))
                    return Response({"Response": response.json(), "status": "failed!"})

                if "token_type" in responseDict:
                    user.tokenType = str(responseDict.get('token_type'))
                if "refresh_token" in responseDict:
                    user.refreshToken = str(responseDict.get('refresh_token'))
                if "expires_in" in responseDict:
                    user.accessTokenExpiration = timezone.now() + timedelta(seconds=responseDict.get('expires_in'))
                user.save()

            if user.accessTokenExpiration is not None:
                if timezone.now() > user.accessTokenExpiration:
                    url = "https://auth.truelayer.com/connect/token"

                    payload = 'grant_type=refresh_token&client_id=' + settings.tl_client_id + '&client_secret=' + settings.tl_client_secret + '&refresh_token=' + user.refreshToken
                    headers = {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                    response = requests.request("POST", url, headers=headers, data=payload)
                    responseDict = response.json()

                    if "access_token" in responseDict:
                        user.accessToken = responseDict.get('access_token')
                        if "token_type" in responseDict:
                            user.tokenType = str(responseDict.get('token_type'))
                        if "refresh_token" in responseDict:
                            user.refreshToken = str(responseDict.get('refresh_token'))
                        if "expires_in" in responseDict:
                            user.accessTokenExpiration = timezone.now() + timedelta(seconds=responseDict.get('expires_in'))
                            user.isBankConnected = True
                            user.save()
                    else:
                        #return Response({"Response": response.json(), "status": "failed!"})
                        if user.refreshToken == None:
                            allTransactionCategories = Utility.getCategoriesForNoBank(True, request)
                        else:
                            allTransactionCategories = Utility.getLastMonthTopCategories(user, request)
                        return Response({"categories": list(allTransactionCategories), "status": "success!",
                                        "new_banks_transactions": 0,
                                        "new_cards_transactions": 0})
                        

                    



            allVendors = Vendor.objects.all()
            auto = ahocorasick.Automaton()
            for vendorObj in allVendors:
                auto.add_word(vendorObj.name.lower(), (vendorObj, vendorObj.name.lower()))
            auto.make_automaton()


            url = "https://api.truelayer.com/data/v1/accounts"
            headers = {
                'Authorization': 'Bearer ' + user.accessToken
            }


            responseAccounts = requests.request("GET", url, headers=headers, data={})
            responseAccountsDict = responseAccounts.json()

            """
            if responseAccountsDict.get('status') != "Succeeded":
                return Response({"Response": responseAccountsDict, "status": "failed!"})
            """

            if "results" in responseAccountsDict:
                accountsDictArr = responseAccountsDict.get('results')
                newBanksTransactions = Utility.handleBanksResponse(accountsDictArr, user, auto)

            """
            CARDS DATA
            """

            url = "https://api.truelayer.com/data/v1/cards"
            headers = {
                'Authorization': 'Bearer ' + user.accessToken
            }

            responseAccounts = requests.request("GET", url, headers=headers, data={})
            responseAccountsDict = responseAccounts.json()

            """
            if responseAccountsDict.get('status') != "Succeeded":
                return Response({"Response": responseAccountsDict, "status": "failed!"})
            """

            if "results" in responseAccountsDict:
                accountsDictArr = responseAccountsDict.get('results')
                newCardsTransactions = Utility.handleCardsResponse(accountsDictArr, user, auto)
            allTransactionCategories = Utility.getLastMonthTopCategories(user, request)

            user.isBankConnected = True
            user.save()
            serializer = UserSerializer(user, many=False, context={"request": request})

            return Response({"categories": list(allTransactionCategories), "status": "success!",
                             "new_banks_transactions": newBanksTransactions,
                             "new_cards_transactions": newCardsTransactions,
                             "user": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getTransactionedCategories(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            if user.isBankConnected == False:
                if user.refreshToken == None:
                    allTransactionCategories = Utility.getCategoriesForNoBank(True, request)
                else:
                    allTransactionCategories = Utility.getLastMonthTopCategories(user, request)
                return Response({"categories": list(allTransactionCategories)})

            is_last_month = False
            if 'is_last_month' in body:
                is_last_month = body['is_last_month']

            allTransactionCategories = Utility.getTransactionCategories(user, request, is_last_month)

            return Response({"categories": list(allTransactionCategories)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getAllTransactionsOfCategory(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            categoryId = body['category_id']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            is_last_month = False
            if 'is_last_month' in body:
                is_last_month = body['is_last_month']

            categories = Category.objects.filter(id=int(categoryId))
            if categories.count() == 0:
                return Response({"status": Values.noCategory_error()})
            category = list(categories)[0]

            today = datetime.datetime.utcnow()
            first_this_month = today.replace(day=1, hour=0, minute=0, second=0,
                                             microsecond=0)  # first date of current month

            last_previous_month = (first_this_month - datetime.timedelta(days=1)).replace(hour=23, minute=59, second=59,
                                                                                 microsecond=999999)
            first_previous_month = last_previous_month.replace(day=1, hour=0, minute=0, second=0, microsecond=0)

            initial_date = first_this_month
            final_date = today

            if is_last_month == True:
                initial_date = first_previous_month
                final_date = last_previous_month



            allTransactions = []

            transactions = Transaction.objects.filter(user=user, transactionType='debit', timestamp__lte=final_date, timestamp__gt=initial_date)

            for transaction in transactions:
                if transaction.category is None:
                    if category.name == 'uncategorized':
                        allTransactions.append(transaction)

                else:
                    if transaction.category.id == category.id:
                        allTransactions.append(transaction)

                    if category.isDuplicateable:
                        if transaction.vendor is not None:
                            if transaction.vendor.category.id == category.id:
                                allTransactions.append(transaction)

            for transaction in allTransactions:
                transaction.amount = float(format(transaction.amount, '.2f'))
            serializer = TransactionSerializer(allTransactions, many=True)

            return Response({"transactions": serializer.data, "count": len(allTransactions)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getHighestRetailer(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            is_last_month = False
            if 'is_last_month' in body:
                is_last_month = body['is_last_month']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            return Utility.getHighestRetailerOfUser(user, is_last_month)
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getMostFrequentShop(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            is_last_month = False
            if 'is_last_month' in body:
                is_last_month = body['is_last_month']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            return Utility.getMostFrequentShopOfUser(user, is_last_month)
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getHihestVendors(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            is_last_month = False
            if 'is_last_month' in body:
                is_last_month = body['is_last_month']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            return Utility.getHighestVendorsOfUser(user, is_last_month)
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getDailyAverageSpend(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            is_last_month = False
            if 'is_last_month' in body:
                is_last_month = body['is_last_month']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            return Utility.getDailyAverageOfUser(user, is_last_month)
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getLastMonthSpend(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            return Utility.lastMonthSpendingUser(user)
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getAllCategories(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})

            categories = Category.objects.all()
            serializer = CategorySerializer(categories, many=True, context={"request": request})

            return Response({"categories": list(serializer.data), "count": len(categories)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def assignCategoryToTransaction(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            categoryId = body['category_id']
            transactionId = body['transaction_id']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})

            categories = Category.objects.filter(id=int(categoryId))
            if categories.count() == 0:
                return Response({"status": Values.noCategory_error()})
            category = list(categories)[0]

            transactions = Transaction.objects.filter(id=int(transactionId))
            if transactions.count() == 0:
                return Response({"status": Values.noTransaction_error()})
            transaction = list(transactions)[0]
            transaction.category = category
            transaction.save()
            serializer = TransactionSerializer(transaction, many=False)
            return Response({"transaction": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getAllTips(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            is_last_month = False
            if 'is_last_month' in body:
                is_last_month = body['is_last_month']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            allTips = []
            
            
            highestRetail = Utility.getHighestRetailerOfUser(user, is_last_month)
            if 'vendor' in highestRetail.data:
                allTips.append({'tip': highestRetail.data['text'] + ' ' + highestRetail.data['vendor']['name']})
            mostFrequentShop = Utility.getMostFrequentShopOfUser(user, is_last_month)
            if 'vendor' in mostFrequentShop.data:
                allTips.append(
                    {'tip': mostFrequentShop.data['text'] + ' ' + mostFrequentShop.data['vendor']['name']})
            else:
                allTips.append({'tip': highestRetail.data['text']})
            
            highestVendors = Utility.getHighestVendorsOfUser(user, is_last_month)
            if 'vendors' in highestVendors.data:
                vendorStr = highestVendors.data['text'] + ' '
                if len(highestVendors.data['vendors']) > 0:
                    vendorStr = vendorStr + highestVendors.data['vendors'][0]['name']
                if len(highestVendors.data['vendors']) > 1:
                    vendorStr = vendorStr + ', ' + highestVendors.data['vendors'][1]['name']
                if len(highestVendors.data['vendors']) > 2:
                    vendorStr = vendorStr + ', ' + highestVendors.data['vendors'][2]['name']

                allTips.append({'tip':  vendorStr})
            else:
                allTips.append({'tip': highestRetail.data['text']})

            
            dailyAverage = Utility.getDailyAverageOfUser(user, is_last_month)
            amount = str('{:,}'.format(dailyAverage.data['value']))
            arr = amount.split(".")
            if len(arr) < 2:
                amount = amount + ".00"
            else:
                if len(arr[1]) < 2:
                    amount = amount + "0"
            allTips.append({'tip': dailyAverage.data['text'] + ' £' + amount})
            

            
            lastMonthSpend = Utility.lastMonthSpendingUser(user)
            words = str(lastMonthSpend.data['text']).split()
            
            amount = str('{:,}'.format(lastMonthSpend.data['value']))
            arr = amount.split(".")
            if len(arr) < 2:
                amount = amount + ".00"
            else:
                if len(arr[1]) < 2:
                    amount = amount + "0"
            allTips.append(
                {'tip': words[0] + ' ' + words[1] + ' ' + words[2] + ' £' + str(amount) +
                        ' ' + words[3] + ' ' + words[4] + ' ' + words[5] + ' ' + words[6]})
            
            staticTips = Tip.objects.all()
            for staticTip in staticTips:
                allTips.append({'tip': staticTip.text})

            

            balance = Utility.fetchBalance(user)
            if balance is not None:
                if balance < 100.0:
                    allTips.append({'tip': Values.lowBanalce_tip()})


            return Response({"Tips": list(allTips)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getAllInsights(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            is_last_month = False
            if 'is_last_month' in body:
                is_last_month = body['is_last_month']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            predictionResponse = Response({"text": Values.prediction_text(), "value": Utility.getPredictionTest(user)["prediction_next_month"]})
            return Response({"highest_retail": Utility.getHighestRetailerOfUser(user, is_last_month).data,
                             "most_frequent_shop": Utility.getMostFrequentShopOfUser(user, is_last_month).data,
                             "highest_vendors": Utility.getHighestVendorsOfUser(user, is_last_month).data,
                             "daily_average": Utility.getDailyAverageOfUser(user, is_last_month).data,
                             "last_month_spend": Utility.lastMonthSpendingUser(user).data,
                             "prediction": predictionResponse.data})

        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def addVendor(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            categoryId = int(body['category_id'])
            allcategories = Category.objects.filter(id=categoryId)
            category = list(allcategories)[0]

            for name in body['names']:
                new = Vendor(category=category,
                             name=str(name).lower())
                new.save()

            vendors = Vendor.objects.filter(category=category).values(*Values.vendor_values())

            return Response({"vendors": list(vendors)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def createNewPot(request):
    if request.method == "POST":
        try:
            auth = request.POST['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            potName = str(request.POST['pot_name'])
            targetAmount = float(request.POST['target_amount'])
            lockForThreeMonths = True
            if request.POST['lock'] == "false":
                lockForThreeMonths = False


            file_data = request.FILES['pot_image']
            if file_data != None:
                new = Pot(potName=potName,
                          targetAmount=targetAmount,
                          lockForThreeMonths=lockForThreeMonths,
                          user=user,
                          potPicture=file_data,
                          potStartingDate=timezone.now())
                new.save()
                serializer = PotSerializer(new, many=False, context={"request": request})

                return Response({"Pot": serializer.data, "status": Values.newPot_success()})
            else:
                return Response({"Error": "No Image!"})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})

@api_view(['POST', ])
def updatePot(request):
    if request.method == "POST":
        try:
            auth = request.POST['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            potName = str(request.POST['pot_name'])
            targetAmount = float(request.POST['target_amount'])
            lockForThreeMonths = True
            if request.POST['lock'] == "false":
                lockForThreeMonths = False

            file_data = request.FILES['pot_image']
            potId = int(request.POST['pot_id'])
            if file_data != None:
                allFoundPots = Pot.objects.filter(id=int(potId), user=user)
                if allFoundPots.count() == 0:
                    return Response({"status": Values.noPot_error()})
                for pot in allFoundPots:
                    pot.potName = potName
                    pot.targetAmount = targetAmount
                    pot.lockForThreeMonths = lockForThreeMonths
                    pot.user = user
                    pot.potPicture = file_data
                    pot.updationDate = timezone.now()

                    pot.save()
                serializer = PotSerializer(list(allFoundPots)[0], many=False, context={"request": request})

                return Response({"Pot": serializer.data, "status": Values.updatePot_success()})
            else:
                return Response({"Error": "No Image!"})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})





@api_view(['POST', ])
def getAllUserPots(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            allFoundPots = Pot.objects.filter(user=user)
            serializer = PotSerializer(allFoundPots, many=True, context={"request": request})

            return Response({"Pots": serializer.data, "count": len(allFoundPots)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def deletePot(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            potId = body['pot_id']
            allFoundPots = Pot.objects.filter(id=int(potId), user=user)
            if allFoundPots.count() == 0:
                return Response({"status": Values.noPot_error()})
            for pot in allFoundPots:
                pot.delete()

            allFoundPots = Pot.objects.filter(user=user)
            serializer = PotSerializer(allFoundPots, many=True, context={"request": request})

            return Response({"Pots": serializer.data, "count": len(allFoundPots), "status": Values.deletePot_success()})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def UpdateUser(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            for i in body:
                if i == "first_name":
                    user.firstName = str(body['first_name'])
                elif i == "sur_name":
                    user.surname = str(body['sur_name'])
                elif i == "nationality":
                    user.nationality = str(body['nationality'])
                elif i == "address":
                    user.address = str(body['address'])
                elif i == "email":
                    user.email = str(body['email'])
                elif i == "dob":
                    dateStr = str(body['dob'])
                    format = '%d-%m-%Y'
                    user.dob = datetime.strptime(dateStr, format).date()
            user.updationDate = timezone.now()
            user.save()

            serializer = UserSerializer(user, many=False, context={"request": request})

            return Response(
                {"User": serializer.data, "status": Values.userUpdated_success()})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def updateUserImage(request):
    if request.method == "POST":
        try:
            auth = request.POST['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            file_data = request.FILES['user_image']
            if file_data != None:
                user.profilePicture = file_data
                user.updationDate = timezone.now()
                user.save()
                serializer = UserSerializer(user, many=False, context={"request": request})

                return Response({"User": serializer.data, "status": Values.userUpdated_success()})
            else:
                return Response({"Error": "No Image!"})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def updateUserImageB64(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            b64_image = body['profilePicture_base64']
            if b64_image != None:
                image_data = base64.b64decode(b64_image)
                filename = 'profilePicture_' + str(user.id) + '.png'
                file_data = ContentFile(image_data, filename)
                user.profilePicture = file_data
                user.updationDate = timezone.now()
                user.save()
                serializer = UserSerializer(user, many=False, context={"request": request})

                return Response({"User": serializer.data, "status": Values.userUpdated_success()})
            else:
                return Response({"Error": "No Image!"})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def getWeeklySpend(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            today = datetime.date.today()
            idx = (today.weekday()) % 7  # MON = 0, SUN = 6 -> SUN = 0 .. SAT = 6
            almostMidnight = datetime.time.max
            finalDate = datetime.datetime.combine(datetime.datetime.today()-datetime.timedelta(days=idx+1), almostMidnight)
            transactions = Transaction.objects.filter(user=user, transactionType='debit',
                                                      timestamp__lte=datetime.datetime.today(), timestamp__gt=finalDate)
            weeklySpend = 0.0
            for transaction in transactions:
                weeklySpend = weeklySpend + (transaction.amount * -1)

            return Response({"weekly_spend": weeklySpend, "count": len(transactions)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})

@api_view(['POST', ])
def getPrediction(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            data = {}
            data['prediction'] = Transaction.get_prediction(user)
            return Response(data=data)
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getPredictionTest(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            data = {}
            data['prediction'] = Utility.getPredictionTest(user)
            return Response(data=data)
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getBalance(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]


            balance = Utility.fetchBalance(user)

            if balance is not None:
                return Response({"balance": balance})

            return Response({"status": "NO RESULTS"})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})

@api_view(['GET', ])
def makeAllVendorsRetailers(request):
    if request.method == "GET":
        try:
            billsCategories = Category.objects.filter(name="bills")
            if billsCategories.count() == 0:
                return Response({"status": Values.noUser_error()})
            billsCategory = list(billsCategories)[0]

            allVendors = Vendor.objects.all()
            count = 0
            for vendor in allVendors:
                if vendor.category.id != billsCategory.id:
                    if not vendor.isRetailer:
                        vendor.isRetailer = True
                        count = count + 1
                        vendor.save()
            return Response({"status": str(str(count) + " vendors made to retailer")})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getAllSavingsFromPots(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            transactionNeededMondays = Utility.calculatePotSavings(user)
            totalSavings = 0.0
            allFoundPotTransactions = PotTransaction.objects.filter(user=user)
            for potTransaction in allFoundPotTransactions:
                totalSavings = totalSavings + potTransaction.amount
            allFoundPots = Pot.objects.filter(user=user)
            return Response({"Saving": totalSavings, "pots_count": len(allFoundPots)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getTransactionsWithoutCategory(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            transactions = Transaction.objects.filter(vendor=None, transactionType='debit').extra(where=["LENGTH(merchantName) > 0 OR LENGTH(metaMerchantName) > 0"])
            for transaction in transactions:
                merchantName = transaction.merchantName
                if len(merchantName) == 0:
                    merchantName = transaction.metaMerchantName
                newVendor = Vendor(name=merchantName.lower(), category=transaction.category)
                newVendor.save()
                transaction.vendor = newVendor
                transaction.save()

            serializer = TransactionSerializer(transactions, many=True)

            return Response({"Transactions": serializer.data, "pots_count": len(transactions)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


            
@api_view(['POST', ])
def fixDuplicateVendors(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            dups = Vendor.objects.values('name').annotate(Count('id')).order_by().filter(id__count__gt=1)
            vendorsLinked = 0
            
            for duplicate in dups:
                vendors = Vendor.objects.filter(name=duplicate['name']).order_by('id')
                for vendor in vendors:
                    if vendors[0].id == vendor.id:
                        print("last vendor "+str(vendor.id))
                    else:
                        transactions = Transaction.objects.filter(vendor=vendor)
                        for transaction in transactions:
                            transaction.vendor == vendors[0]
                            transaction.category = vendor.category
                            transaction.save()
                        vendorsLinked = vendorsLinked + len(transactions)
                        vendor.delete()
            
            transactions = Transaction.objects.filter(vendor=None, transactionType='debit').extra(where=["LENGTH(merchantName) > 0 OR LENGTH(metaMerchantName) > 0"])
            
            changedTransaction = []
            for transaction in transactions:
                merchantName = transaction.merchantName
                if len(merchantName) == 0:
                    merchantName = transaction.metaMerchantName
                vendors = Vendor.objects.filter(name=merchantName.lower())
                if vendors.count() > 0:
                    vendor = vendors[0]
                    transaction.vendor = vendor
                    transaction.category = vendor.category
                    transaction.save()
                    changedTransaction.append(transaction.id)
                
                
                

        
            #vendors = Vendor.objects.filter(name=dups[0]['name']).order_by('id')
            #duplicateVendors = Vendor.objects.filter(name__in=dups)

            #serializer = VendorSerializer(vendors, many=True)

            return Response({"duplicate_vendors": dups, "transactions_count": vendorsLinked, "changed_transaction": changedTransaction})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def fixVendorWithName(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            vendorName = body['vendor']

            allFoundVendors = Vendor.objects.filter(name__iexact=str(vendorName).lower()).order_by('id')
            if allFoundVendors.count() > 1:
                vendorMain = None
                transactionsCount = 0
                for vendor in allFoundVendors:
                    if vendor.category != None:
                        vendorMain = vendor
                        break
                if vendorMain == None:
                    vendorMain = list(allFoundVendors)[0]
                for vendor in allFoundVendors:
                    if vendor != vendorMain:
                        transactions = Transaction.objects.filter(vendor=vendor)
                        transactionsCount = transactionsCount + len(transactions)
                        for transaction in transactions:
                            transaction.vendor == vendorMain
                            transaction.category = vendorMain.category
                            transaction.save()
                        vendor.delete()

            return Response({"changed_transaction": transactionsCount, "vendors_count" : len(allFoundVendors)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})

@api_view(['POST', ])
def fixVendorWithName(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            vendorName = body['vendor']

            allFoundVendors = Vendor.objects.filter(name__iexact=str(vendorName).lower()).order_by('id')
            if allFoundVendors.count() > 1:
                vendorMain = None
                transactionsCount = 0
                for vendor in allFoundVendors:
                    if vendor.category != None:
                        vendorMain = vendor
                        break
                if vendorMain == None:
                    vendorMain = list(allFoundVendors)[0]
                for vendor in allFoundVendors:
                    if vendor != vendorMain:
                        transactions = Transaction.objects.filter(vendor=vendor)
                        transactionsCount = transactionsCount + len(transactions)
                        for transaction in transactions:
                            transaction.vendor == vendorMain
                            transaction.category = vendorMain.category
                            transaction.save()
                        vendor.delete()

            return Response({"changed_transaction": transactionsCount, "vendors_count" : len(allFoundVendors)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})@api_view(['POST', ])


@api_view(['POST', ])
def fixTransactionCategory(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            unCategorizedCategories = Category.objects.filter(name__iexact='uncategorized')
            unCategorizedCategory = list(unCategorizedCategories)[0]
            transactions = Transaction.objects.filter(category=unCategorizedCategory, transactionType='debit').extra(where=["LENGTH(transactionClassificationString) > 2"])
            changedTransactionsCount = 0
            changedTransactions = []
            for transaction in transactions:
                transactionClassificationString = transaction.transactionClassificationString
                transactionClassificationString = transactionClassificationString.replace(']','').replace('[','')
                transactionClassificationString = transactionClassificationString.replace("'","")
                transactionClassifications = transactionClassificationString.split(",")
                for transactionClassification in reversed(transactionClassifications):
                        allFoundSubCats = Subcategory.objects.filter(
                            name__iexact=str(transactionClassification).lower())
                        if allFoundSubCats.count() > 0:
                            subCat = list(allFoundSubCats)[0]
                            transaction.subcategory = subCat
                            transaction.category = subCat.category
                            transaction.save()
                            changedTransactions.append(transaction)
                            changedTransactionsCount = changedTransactionsCount + 1
                            break
            
            serializer = TransactionSerializer(changedTransactions, many=True)
            return Response({"changed_transaction": changedTransactionsCount, "all_transactions" : len(transactions), "transactions" : serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getTwilioRecording(request):
    if request.method == "POST":
        try:
            recordingUrl = str(request.POST['RecordingUrl'])
            
            newKey = Key(key="URL", text=recordingUrl)
            newKey.save()


            return Response({"Recording": recordingUrl})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def getTwilioCallStatus(request):
    if request.method == "POST":
        try:
            
            dialCallStatus = "no-status"
            dialCallSid = "no-call-sid"
            dialCallDuration = "0.0"
            recordingUrl = ""

            if 'DialCallStatus' in request.POST:
                dialCallStatus = str(request.POST['DialCallStatus'])
            if 'CallSid' in request.POST:
                dialCallSid = str(request.POST['CallSid'])
            if 'DialCallDuration' in request.POST:
                dialCallDuration = str(request.POST['DialCallDuration'])
            if 'RecordingUrl' in request.POST:
                recordingUrl = str(request.POST['RecordingUrl'])


            newKey = Key(key=dialCallSid + "<->" + dialCallStatus + "<->" + dialCallDuration, text=recordingUrl)
            newKey.save()

            return Response({"Status_Recording": dialCallStatus+"<->"+recordingUrl})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def calculatePotSaving(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            transactionNeededMondays = Utility.calculatePotSavings(user)

            return Response({"dates": transactionNeededMondays})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getSavingThisMonth(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            totalSavings = Utility.getSavingsThisMonth(user)

            return Response({"Saving": totalSavings})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def getAllPotTransactions(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            
            allFoundPotTransactions = PotTransaction.objects.filter(user=user).order_by('transactionDate')
            serializer = PotTransactionSerializer(allFoundPotTransactions, many=True)
            return Response({"Pot_transactions": serializer.data, "transactions_count": len(allFoundPotTransactions)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def saveMonthlyDebit(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            income = float(body['income'])
            bills = float(body['bills'])
            transport = float(body['transport'])
            groceries = float(body['groceries'])
            retail = float(body['retail'])
            eating = float(body['eating'])

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            today = timezone.now()
            first_this_month = today.replace(day=1,hour=0, minute=0, second=0, microsecond=0)
            currentDate = first_this_month

            updationDate = today

            allMonthlyDebits = MonthlyDebit.objects.filter(user=user)
            if allMonthlyDebits.count() == 0:
                creationDate = today
                monthlyDebit = MonthlyDebit(user=user,
                                income=income,
                                currentDate=currentDate,
                                updationDate=updationDate,
                                creationDate=creationDate)
            else:
                monthlyDebit = list(allMonthlyDebits)[0]
                monthlyDebit.income=income
                monthlyDebit.currentDate=currentDate
                monthlyDebit.updationDate=updationDate

            monthlyDebit.save()
                

            billsCategory = Category.objects.filter(name='bills')
            transportCategory = Category.objects.filter(name='transport')
            groceriesCategory = Category.objects.filter(name='groceries')
            retailCategory = Category.objects.filter(name='retail & homeware')
            eatingCategory = Category.objects.filter(name='eating out')


            allBillsDebits = MonthlyDebitCategory.objects.filter(category=list(billsCategory)[0],monthlyDebit=monthlyDebit)
            if allBillsDebits.count() == 0:
                billsDebit = MonthlyDebitCategory(category=list(billsCategory)[0],
                                monthlyDebit=monthlyDebit,
                                targetAmount=bills,
                                updationDate=updationDate,
                                creationDate=creationDate)
            else:
                billsDebit = list(allBillsDebits)[0]
                billsDebit.targetAmount=bills
                billsDebit.updationDate=updationDate
            billsDebit.save()



            alltransportDebits = MonthlyDebitCategory.objects.filter(category=list(transportCategory)[0],monthlyDebit=monthlyDebit)
            if alltransportDebits.count() == 0:
                transportDebit = MonthlyDebitCategory(category=list(transportCategory)[0],
                                monthlyDebit=monthlyDebit,
                                targetAmount=transport,
                                updationDate=updationDate,
                                creationDate=creationDate)
            else:
                transportDebit = list(alltransportDebits)[0]
                transportDebit.targetAmount=transport
                transportDebit.updationDate=updationDate
            transportDebit.save()




            allgroceriesDebits = MonthlyDebitCategory.objects.filter(category=list(groceriesCategory)[0],monthlyDebit=monthlyDebit)
            if allgroceriesDebits.count() == 0:
                groceriesDebit = MonthlyDebitCategory(category=list(groceriesCategory)[0],
                                monthlyDebit=monthlyDebit,
                                targetAmount=groceries,
                                updationDate=updationDate,
                                creationDate=creationDate)
            else:
                groceriesDebit = list(allgroceriesDebits)[0]
                groceriesDebit.targetAmount=groceries
                groceriesDebit.updationDate=updationDate
            groceriesDebit.save()



            allretailDebits = MonthlyDebitCategory.objects.filter(category=list(retailCategory)[0],monthlyDebit=monthlyDebit)
            if allretailDebits.count() == 0:
                retailDebit = MonthlyDebitCategory(category=list(retailCategory)[0],
                                monthlyDebit=monthlyDebit,
                                targetAmount=retail,
                                updationDate=updationDate,
                                creationDate=creationDate)
            else:
                retailDebit = list(allretailDebits)[0]
                retailDebit.targetAmount=retail
                retailDebit.updationDate=updationDate
            retailDebit.save()



            alleatingDebits = MonthlyDebitCategory.objects.filter(category=list(eatingCategory)[0],monthlyDebit=monthlyDebit)
            if alleatingDebits.count() == 0:
                eatingDebit = MonthlyDebitCategory(category=list(eatingCategory)[0],
                                monthlyDebit=monthlyDebit,
                                targetAmount=eating,
                                updationDate=updationDate,
                                creationDate=creationDate)
            else:
                eatingDebit = list(alleatingDebits)[0]
                eatingDebit.targetAmount=eating
                eatingDebit.updationDate=updationDate
            eatingDebit.save()


            return Utility.getUserDebitsCredits(user, request)
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def getMonthlyDebit(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
        

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            return Utility.getUserDebitsCredits(user, request)
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def saveMonthlyCredits(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]


            allMonthlyCredits = MonthlyCredit.objects.filter(user=user)
            for monthlyCredit in allMonthlyCredits:
                monthlyCredit.delete()

            today = timezone.now()
            first_this_month = today.replace(day=1,hour=0, minute=0, second=0, microsecond=0)
            currentDate = first_this_month

            updationDate = today

            if "credits" in body:
                credits = body['credits']
                for credit in credits:
                    creditName = credit['credit_name']
                    amountLeft = credit['amount_left']
                    newMonthlyCredit = MonthlyCredit(creditName=str(creditName),
                                     amountLeft=float(amountLeft),
                                     user=user,
                                     currentDate=currentDate,
                                     updationDate=updationDate,
                                     creationDate=updationDate)
                    newMonthlyCredit.save()

            return Utility.getUserDebitsCredits(user, request)
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def registerUserDeviceToken(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            device_token = body['device_token']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            
            user.deviceToken = device_token
            user.save()
            
            serializer = UserSerializer(user, many=False, context={"request": request})

            return Response({"user": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def getAllUserUbuntus(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            type = body['type']
            ubuntu_id = request.GET.get('ubuntu_id')

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            
            if type == "created":
                allFoundUbuntus = Ubuntu.objects.filter(creator=user).order_by('creationDate')
            elif type == "joined":
                allFoundUbuntus_ids = [i.id for i in Ubuntu.objects.all().exclude(creator=user) if i.is_member(user) or i.is_joined(user)]
                allFoundUbuntus = Ubuntu.objects.filter(id__in=allFoundUbuntus_ids)
            else:
                return Response({"status": "Unknown ubuntu type. choices are (created, joined)."})

            if ubuntu_id:
                ubuntu = allFoundUbuntus.filter(id=ubuntu_id).first()
                if not ubuntu:
                    return Response({"status": "Ubuntu doesn't exist"})

                serializer = UbuntuSerializer(ubuntu)
                return Response({"ubuntu": serializer.data})
                
            
            serializer = UbuntuSerializer(allFoundUbuntus, many=True)
            return Response({"ubuntus": serializer.data, "ubuntus_count": len(allFoundUbuntus)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def saveUbuntu(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            name = body['name']
            savingGoal = float(body['savingGoal'])
            monthlyContribution = float(body['monthlyContribution'])
            numberOfParticipants = int(body['numberOfParticipants'])
            date = str(body['date'])
            
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            today = timezone.now()
            updationDate = today

            newUbuntu = Ubuntu(
                creator=user,
                name=name,
                savingGoal=savingGoal,
                monthlyContribution=monthlyContribution,
                numberOfParticipants=numberOfParticipants,
                date=date,
                updationDate=updationDate,
                creationDate=today
            )
            newUbuntu.save()

            _thisUbuntu = newUbuntu

            
            max_invites = _thisUbuntu.numberOfParticipants
            invitedUsers = User.objects.filter(id__in=body['invites']).exclude(id=user.id)
            invitedPhoneNumbers = body['phoneNumberInvites']
            invitedPhoneNumbersList = [i.get('phoneNumber') for i in invitedPhoneNumbers]
            existing_invites = UbuntuInvite.objects.filter(ubuntu=_thisUbuntu).exclude(id__in=invitedUsers)
            existing_phoneNunber_invites = UbuntuPhoneNumberInvite.objects.filter(ubuntu=_thisUbuntu).exclude(phoneNumber__in=invitedPhoneNumbersList)
            num_invites = invitedUsers.count() + len(invitedPhoneNumbers) + existing_invites.count() + existing_phoneNunber_invites.count()
            if num_invites > max_invites:
                return Response({"Error": f"Number of invitations ({num_invites}) exeeded the limit ({max_invites})"})

            for invitedUser in invitedUsers:
                if UbuntuInvite.objects.filter(ubuntu=_thisUbuntu, user=invitedUser).exists():
                    continue
                new = UbuntuInvite(
                    user=invitedUser,
                    ubuntu=_thisUbuntu,
                    status="invited",
                    updationDate=updationDate,
                    creationDate=today
                )
                new.save()


            for invitedPhoneNumber in invitedPhoneNumbers:
                phoneNumber = invitedPhoneNumber.get('phoneNumber')
                username = invitedPhoneNumber.get('username')
                if user.phone == phoneNumber:
                    continue

                Utility.send_ubuntu_to_phone(phoneNumber, user.firstName, _thisUbuntu.name)
                if User.objects.filter(phone=phoneNumber).exists():
                    invitedUser = User.objects.filter(phone=phoneNumber).first()
                    if UbuntuInvite.objects.filter(ubuntu=_thisUbuntu, user=invitedUser).exists():
                        continue
                    new = UbuntuInvite(
                        user=invitedUser,
                        ubuntu=_thisUbuntu,
                        status="invited",
                        updationDate=updationDate,
                        creationDate=today
                    )
                    new.save()
                else:
                    if UbuntuPhoneNumberInvite.objects.filter(ubuntu=_thisUbuntu, phoneNumber=phoneNumber).exists():
                        continue
                    new = UbuntuPhoneNumberInvite(
                        username = username,
                        phoneNumber=phoneNumber,
                        ubuntu=_thisUbuntu,
                        status="invited",
                        updationDate=updationDate,
                        creationDate=today
                    )
                    new.save()

            serializer = UbuntuSerializer(newUbuntu)
            return Response({"ubuntu": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def sendUbuntuInvitation(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            ubuntu_id = body['ubuntu_id']
            
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            today = timezone.now()
            updationDate = today

            _thisUbuntu = Ubuntu.objects.filter(creator=user, id=ubuntu_id)
            if not _thisUbuntu.exists():
                return Response({"status": "Ubuntu doesn't exist"})

            _thisUbuntu = _thisUbuntu.first()

            if _thisUbuntu.isStarted:
                return Response({"status": "Ubuntu has already started"})            

            max_invites = _thisUbuntu.numberOfParticipants
            invitedUsers = User.objects.filter(id__in=body['invites']).exclude(id=user.id)
            invitedPhoneNumbers = body['phoneNumberInvites']
            invitedPhoneNumbersList = [i.get('phoneNumber') for i in invitedPhoneNumbers]
            existing_invites = UbuntuInvite.objects.filter(ubuntu=_thisUbuntu).exclude(id__in=invitedUsers)
            existing_phoneNunber_invites = UbuntuPhoneNumberInvite.objects.filter(ubuntu=_thisUbuntu).exclude(phoneNumber__in=invitedPhoneNumbersList)
            num_invites = invitedUsers.count() + len(invitedPhoneNumbers) + existing_invites.count() + existing_phoneNunber_invites.count()
            if num_invites > max_invites:
                return Response({"Error": f"Number of invitations ({num_invites}) exeeded the limit ({max_invites})"})

            for invitedUser in invitedUsers:
                if UbuntuInvite.objects.filter(ubuntu=_thisUbuntu, user=invitedUser).exists():
                    continue
                new = UbuntuInvite(
                    user=invitedUser,
                    ubuntu=_thisUbuntu,
                    status="invited",
                    updationDate=updationDate,
                    creationDate=today
                )
                new.save()


            for invitedPhoneNumber in invitedPhoneNumbers:
                phoneNumber = invitedPhoneNumber.get('phoneNumber')
                username = invitedPhoneNumber.get('username')
                if user.phone == phoneNumber:
                    continue
                if User.objects.filter(phone=phoneNumber).exists():
                    invitedUser = User.objects.filter(phone=phoneNumber).first()
                    if UbuntuInvite.objects.filter(ubuntu=_thisUbuntu, user=invitedUser).exists():
                        continue
                    new = UbuntuInvite(
                        user=invitedUser,
                        ubuntu=_thisUbuntu,
                        status="invited",
                        updationDate=updationDate,
                        creationDate=today
                    )
                    new.save()
                else:
                    if UbuntuPhoneNumberInvite.objects.filter(ubuntu=_thisUbuntu, phoneNumber=phoneNumber).exists():
                        continue
                    new = UbuntuPhoneNumberInvite(
                        username = username,
                        phoneNumber=phoneNumber,
                        ubuntu=_thisUbuntu,
                        status="invited",
                        updationDate=updationDate,
                        creationDate=today
                    )
                    new.save()

            serializer = UbuntuSerializer(_thisUbuntu)
            return Response({"ubuntu": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def getUbuntuInvitations(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            today = timezone.now()
            updationDate = today

            ubuntuInvitations = UbuntuInvite.objects.filter(user=user, status="invited", ubuntu__in=Ubuntu.objects.filter(isStarted=False))
            serializer = UbuntuInviteSerializer(ubuntuInvitations, many=True)
            return Response({"invitations": serializer.data, "ubuntus_invitations_count": len(ubuntuInvitations)})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})


@api_view(['POST', ])
def answerUbuntuInvitation(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            invitation_id = body['invitation_id']
            status = body['status']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            invitation = UbuntuInvite.objects.filter(user=user, id=invitation_id)

            if not invitation.exists():
                return Response({"Error": "Invitation doesn't exist"})

            invitation = invitation.first()

            if invitation.ubuntu.isStarted:
                return Response({"Error": "Ubuntu has already started"})
            
            invitation.status = status
            invitation.save()

            serializer = UbuntuInviteSerializer(invitation)
            return Response({"invitation": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def startUbuntu(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            ubuntu_id = body['ubuntu_id']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            ubuntu = Ubuntu.objects.filter(creator=user, id=ubuntu_id)
            if not ubuntu.exists():
                return Response({"Error": "Ubuntu doesn't exist"})

            ubuntu = ubuntu.first()

            if ubuntu.isStarted:
                return Response({"status": "Ubuntu has already started"})
            
            ubuntu.isStarted = True
            ubuntu.save()


            creator_member = UbuntuMember(
                user = ubuntu.creator,
                ubuntu = ubuntu
            )
            creator_member.save()
            for invitation in UbuntuInvite.objects.filter(ubuntu=ubuntu, status="joined"):
                member = UbuntuMember(
                    user = invitation.user,
                    ubuntu = ubuntu
                )
                member.save()

            members = UbuntuMember.objects.filter(ubuntu=ubuntu)
            indexes = list(range(1, members.count()+1))

            for member in members:
                index = choice(indexes)
                member.index = index
                member.save()
                indexes.remove(index)

            serializer = UbuntuSerializer(ubuntu)
            return Response({"ubuntu": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def sendUbuntuTransaction(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            transaction_id = body['transaction_id']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            transaction = UbuntuTransaction.objects.filter(sender=user, id=transaction_id)
            if not transaction.exists():
                return Response({"Error": "Transaction doesn't exist"})

            transaction = transaction.first()

            transaction.is_sent = True
            transaction.save()

            serializer = UbuntuSerializer(transaction.ubuntu)
            return Response({"ubuntu": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def receiveUbuntuTransaction(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            transaction_id = body['transaction_id']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            transaction = UbuntuTransaction.objects.filter(receiver=user, id=transaction_id)
            if not transaction.exists():
                return Response({"Error": "Transaction doesn't exist"})

            transaction = transaction.first()

            transaction.is_received = True
            transaction.save()

            serializer = UbuntuSerializer(transaction.ubuntu)
            return Response({"ubuntu": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def sendUbuntuShiftRequest(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            ubuntu_id = body['ubuntu_id']
            member_id = body['member_id']
            receiver_id = body['receiver_id']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            ubuntu = Ubuntu.objects.filter(id=ubuntu_id).first()
            if not ubuntu:
                return Response({"Error": "Ubuntu doesn't exist"})

            sender = UbuntuMember.objects.filter(ubuntu=ubuntu, user=user, id=member_id).first()
            receiver = UbuntuMember.objects.filter(ubuntu=ubuntu, id=receiver_id).first()
            if not sender or not receiver:
                return Response({"Error": "members don't exist"})
            
            shiftRequest = UbuntuShiftRequest(
                sender = sender,
                receiver = receiver,
                ubuntu = ubuntu
            )
            shiftRequest.save()
            return Response({"success": "Shift request is sent successfully!"})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def getUbuntuShiftRequests(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            ubuntu_id = body['ubuntu_id']
            type = body['request_type']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            ubuntu = Ubuntu.objects.filter(id=ubuntu_id).first()
            if not ubuntu:
                return Response({"Error": "Ubuntu doesn't exist"})

            member = UbuntuMember.objects.filter(ubuntu=ubuntu, user=user).first()
            if not member:
                return Response({"Error": "You are not a member of this ubuntu"})
            
            shiftRequest = UbuntuShiftRequest.objects.filter(
                ubuntu = ubuntu
            )
            if type == "sent":
                shiftRequest = shiftRequest.filter(sender=member)
            elif type == "received":
                shiftRequest = shiftRequest.filter(receiver=member)
            else:
                return Response({"Error": "Unknown shift request type. Choices are (sent, received)."})

            
            serializer = UbuntuShiftRequestSerializer(shiftRequest, many=True)
            return Response({"requests": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def answerUbuntuShiftRequest(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            ubuntu_id = body['ubuntu_id']
            shiftRequest_id = body['shiftRequest_id']
            answer = body['answer']

            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]

            ubuntu = Ubuntu.objects.filter(id=ubuntu_id).first()
            if not ubuntu:
                return Response({"Error": "Ubuntu doesn't exist"})

            member = UbuntuMember.objects.filter(ubuntu=ubuntu, user=user).first()
            if not member:
                return Response({"Error": "You are not a member of this ubuntu"})
            
            shiftRequest = UbuntuShiftRequest.objects.filter(
                id = shiftRequest_id,
                ubuntu = ubuntu,
                receiver = member
            ).first()
            
            if not shiftRequest:
                return Response({"Error": "Shift request doesn't exist"})
            
            if shiftRequest.status != "pending":
                return Response({"Error": f"Shift request is already {shiftRequest.status}"})

            if answer == "accepted":
                shiftRequest.status = "accepted"
                shiftRequest.save()
                
                sender = shiftRequest.sender
                receiver = shiftRequest.receiver
                
                holder = receiver.index
                
                receiver.index = sender.index
                receiver.save()
                
                sender.index = holder
                sender.save()


            elif type == "rejected":
                shiftRequest.status = "rejected"
                shiftRequest.save()

            else:
                return Response({"Error": "Unknown answer. Choices are (accepted, rejected)."})

            
            serializer = UbuntuShiftRequestSerializer(shiftRequest)
            return Response({"request": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def getAllUsersForInvite(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]


            allUsers = User.objects.exclude(id=user.id)

            
            serializer = UserInviteSerializer(allUsers, many=True, context={"request": request})
            return Response({"users": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})





@api_view(['POST', ])
def uploadChatRecord(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            allFoundUsers = User.objects.filter(authToken=str(auth))
            if allFoundUsers.count() == 0:
                return Response({"status": Values.noUser_error()})
            user = list(allFoundUsers)[0]
            b64_record = body['b64_record']
            if b64_record != None:
                record_data = base64.b64decode(b64_record)
                filename = 'record_' + str(user.id) + '.mp3'
                file_data = ContentFile(record_data, filename)
                record = Record(
                    file = file_data
                )
                record.save()
                return Response({"record_id": record.id})
            else:
                return Response({"Error": "No Image!"})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})




@api_view(['POST', ])
def getChatHistory(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            auth = body['auth_token']
            ubuntu_id = body['ubuntu_id']
            min_id = request.GET.get('min_id')

            user = User.objects.filter(authToken=str(auth)).first()
            if not user:
                return Response({"status": Values.noUser_error()})

            ubuntu = Ubuntu.objects.filter(id=ubuntu_id).first()
            if not ubuntu:
                return Response({"status": "Ubuntu doesn't exist"})

            if not UbuntuMember.objects.filter(user=user, ubuntu=ubuntu).exists():
                return Response({"status": "You are not a member of this ubuntu!"})

            chatMessages = ChatMessage.objects.filter(ubuntu=ubuntu)
            if min_id:
                chatMessages = chatMessages.filter(id_lt=min_id)
            
            chatMessages = chatMessages.order_by('-id')[:10]
            serializer = ChatMessageSerializer(chatMessages, many=True)
            return Response({"messages": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})



@api_view(['POST', ])
def testUsers(request):
    if request.method == "POST":
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)

            
            allUsers = []
            for inviteId in body['invites']:
                invitedUsers = User.objects.filter(id=inviteId)
                for user in invitedUsers:
                    allUsers.append(user)
            

            

            serializer = UserInviteSerializer(allUsers, many=True, context={"request": request})
            return Response({"users": serializer.data})
        except KeyError as e:
            return Response({"Error": "KeyError " + str(e)})
        except ValueError as e:
            return Response({"Error": "ValueError " + str(e)})
        except Exception as e:
            return Response({"Error": str(e)})