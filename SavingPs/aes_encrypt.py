import hashlib, os

class SQBCipher:

    def __init__(self, key):
        self.key = hashlib.sha256(key.encode()).digest()

    def encrypt(raw):
        password_salt = 'ibuildx'
        password = raw

        hash = hashlib.sha512()
        hash.update(('%s%s' % (password_salt, password)).encode('utf-8'))
        return hash.hexdigest()


