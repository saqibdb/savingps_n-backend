from rest_framework import serializers
from SavingPs.models import User, Bank, Category, Subcategory, Vendor, Transaction, Card, Pot, Tip, Key, PotTransaction, MonthlyDebit, MonthlyDebitCategory, MonthlyCredit
from SavingPs.models import Ubuntu, UbuntuPhoneNumberInvite, UbuntuInvite, UbuntuMember, UbuntuShiftRequest, ChatMessage
from django.conf import settings
import os


def test_validation(value):
    if value == 'DEBIT':
        return 'D'
    else:
        return 'C'


class UserSerializer(serializers.ModelSerializer):
    profilePicture_url = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'authToken', 'deviceToken', 'creationDate', 'email', 'lastLoginDate', 'phone', 'updationDate',
                  'verificationCodePhone', 'isPhoneVerified', 'firstName', 'surname', 'address', 'profilePicture_url',
                  'dob', 'nationality', 'isAccountBalanceShow', 'isBankConnected')


    def get_profilePicture_url(self, user):
        request = self.context.get('request')
        if user.profilePicture and hasattr(user.profilePicture, 'url'):
            photo_url = user.profilePicture.url
            return os.path.join(settings.SITE_DOMAIN, photo_url)
        else:
            return None

class VendorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vendor
        fields = ('id', 'name', 'category', 'isRetailer')

class CategorySerializer(serializers.ModelSerializer):
    categoryImage_url = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = ('id', 'name', 'categoryImage_url', 'initialColor', 'middleColor', 'finalColor', 'isDuplicateable')


    def get_categoryImage_url(self, category):
        request = self.context.get('request')
        if category.categoryImage and hasattr(category.categoryImage, 'url'):
            photo_url = category.categoryImage.url
            return request.build_absolute_uri(photo_url)
        else:
            return None


class TransactionSerializer(serializers.ModelSerializer):
    vendor = VendorSerializer()

    class Meta:
        model = Transaction
        fields = ('id', 'category', 'vendor', 'user', 'bank', 'card', 'timestamp', 'description',
                  'transactionType', 'merchantName', 'amount', 'currency')

class PotSerializer(serializers.ModelSerializer):
    potImage_url = serializers.SerializerMethodField('get_potImage_url')
    totalSavingAmount = serializers.SerializerMethodField('get_totalSavingAmount')
    weeklyInstallmentAmount = serializers.ReadOnlyField(default=0.0)
    class Meta:
        model = Pot
        fields = ('id', 'potName', 'potImage_url', 'targetAmount','totalSavingAmount','weeklyInstallmentAmount', 'lockForThreeMonths', 'potStartingDate', 'isCompleted')


    def get_potImage_url(self, pot):
        request = self.context.get('request')
        if pot.potPicture and hasattr(pot.potPicture, 'url'):
            photo_url = pot.potPicture.url
            return request.build_absolute_uri(photo_url)
        else:
            return None

    def get_totalSavingAmount(self, pot):
        potSaving = 0.0
        allFoundPotTransactions = PotTransaction.objects.filter(pot=pot)
        for potTransaction in allFoundPotTransactions:
            potSaving = potSaving + potTransaction.amount
        return potSaving


class PotTransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = PotTransaction
        fields = ('id', 'user', 'pot', 'amount', 'transactionDate', 'creationDate')



class MonthlyDebitSerializer(serializers.ModelSerializer):

    class Meta:
        model = MonthlyDebit
        fields = ('id', 'user', 'income', 'currentDate', 'creationDate', 'updationDate')



class MonthlyDebitCategorySerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = MonthlyDebitCategory
        fields = ('id', 'category', 'monthlyDebit', 'targetAmount', 'creationDate', 'updationDate')


class MonthlyCreditSerializer(serializers.ModelSerializer):

    class Meta:
        model = MonthlyCredit
        fields = ('id', 'user', 'creditName', 'amountLeft', 'currentDate', 'creationDate', 'updationDate')



class UserInviteSerializer(serializers.ModelSerializer):
    profilePicture_url = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'phone', 'firstName', 'surname', 'profilePicture_url')


    def get_profilePicture_url(self, user):
        request = self.context.get('request')
        if user.profilePicture and hasattr(user.profilePicture, 'url'):
            photo_url = user.profilePicture.url
            return os.path.join(settings.SITE_DOMAIN, photo_url)
        else:
            return None



class UbuntuMinSerializer(serializers.ModelSerializer):

    creator = UserInviteSerializer()

    class Meta:
        model = Ubuntu
        fields = ('id', 'creator', 'name')


class UbuntuInviteSerializer(serializers.ModelSerializer):

    ubuntu = UbuntuMinSerializer()
    user = UserInviteSerializer()

    class Meta:
        model = UbuntuInvite
        fields = ('id', 'ubuntu', 'user', 'status', 'creationDate', 'updationDate')




class UbuntuPhoneNumberInviteSerializer(serializers.ModelSerializer):

    class Meta:
        model = UbuntuPhoneNumberInvite
        fields = ('id', 'phoneNumber', 'username', 'creationDate', 'updationDate')



class UbuntuMemberSerializer(serializers.ModelSerializer):

    user = UserInviteSerializer()

    class Meta:
        model = UbuntuMember
        fields = ('id', 'user', 'index')

        
class UbuntuSerializer(serializers.ModelSerializer):
    ubuntu_invites = serializers.SerializerMethodField('get_ubuntuInvites')
    ubuntu_phone_number_invites = serializers.SerializerMethodField('get_ubuntuPhoneNumberInvites')
    ubuntu_members = serializers.SerializerMethodField('get_ubuntuMembers')

    class Meta:
        model = Ubuntu
        fields = ('id', 'creator', 'name', 'ubuntu_invites', 'ubuntu_phone_number_invites', 'ubuntu_members', 'savingGoal', 'monthlyContribution', 'numberOfParticipants', 'isStarted', 'date', 'creationDate', 'updationDate')

    def get_ubuntuInvites(self, ubuntu):
        allFoundUbuntuInvites = UbuntuInvite.objects.filter(ubuntu=ubuntu)
        return UbuntuInviteSerializer(allFoundUbuntuInvites, many=True).data

    
    def get_ubuntuPhoneNumberInvites(self, ubuntu):
        allFoundUbuntuPhoneNumberInvites = UbuntuPhoneNumberInvite.objects.filter(ubuntu=ubuntu)
        return UbuntuPhoneNumberInviteSerializer(allFoundUbuntuPhoneNumberInvites, many=True).data

    def get_ubuntuMembers(self, ubuntu):
        allFoundUbuntuMembers = UbuntuMember.objects.filter(ubuntu=ubuntu)
        return UbuntuMemberSerializer(allFoundUbuntuMembers, many=True).data


class UbuntuShiftRequestSerializer(serializers.ModelSerializer):
    sender = UbuntuMemberSerializer()
    receiver = UbuntuMemberSerializer()
    ubuntu = UbuntuSerializer()

    class Meta:
        model = UbuntuShiftRequest
        fields = "__all__"




class ChatMessageSerializer(serializers.ModelSerializer):

    msg_sender = UserInviteSerializer()
    record = serializers.SerializerMethodField('get_record')

    class Meta:
        model = ChatMessage
        fields = "__all__"

    def get_record(self, chatMsg):
        if chatMsg.record:
            return chatMsg.record.file.url
        return None