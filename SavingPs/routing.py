from django.urls import re_path
from django.conf.urls import url

from .consumers import (
    shiftRequest,
    groupChat,
)

websocket_urlpatterns = [
    url(r'shift-request/(?P<sender_id>\w+)/(?P<rec_id>\w+)/$', shiftRequest.as_asgi()),
    url(r'group-chat/(?P<ubuntu_id>\w+)/$', groupChat.as_asgi()),
]