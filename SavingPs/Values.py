from SavingPs_N import settings
from SavingPs.models import Constant


def noUser_error():
    texts = Constant.objects.filter(key="no_user_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "No User found!"

def noPot_error():
    texts = Constant.objects.filter(key="no_pot_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "No Pot found!"

def defaultPot_error():
    texts = Constant.objects.filter(key="default_pot_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Default Pot cannot be deleted!"


def wrongCode_error():
    texts = Constant.objects.filter(key="wrong_code_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "The Code is incorrect!"

def anotherUser_error():
    texts = Constant.objects.filter(key="another_user_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Another User with same email already exisits!"

def phoneVerified_error():
    texts = Constant.objects.filter(key="phone_verified_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Phone number is not verified yet!"

def noUserPhone_error():
    texts = Constant.objects.filter(key="no_user_phone_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "No user registered with this phone number. Please sign up!"

def wrongPin_error():
    texts = Constant.objects.filter(key="wrong_pin_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Pin entered is incorrect!"

def noCategory_error():
    texts = Constant.objects.filter(key="no_category_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "No Category found!"

def noTransaction_error():
    texts = Constant.objects.filter(key="no_transaction_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "No Transaction found!"

def noBankAndCard_error():
    texts = Constant.objects.filter(key="no_bank_and_card_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "No Bank Account and card present to get balance!"

def noHighestRetailer_error():
    texts = Constant.objects.filter(key="no_highest_retailer_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "No Retailer found in transactions!"

def noMostFrequentShop_error():
    texts = Constant.objects.filter(key="no_most_frequent_shop_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "No shop found in transactions!"

def noHighestVendors_error():
    texts = Constant.objects.filter(key="no_highest_vendors_error")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "No vendors found in transactions!"


def highestRetailer_text():
    texts = Constant.objects.filter(key="highest_retailer_text")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Highest retail spend"

def mostFrequentShop_text():
    texts = Constant.objects.filter(key="most_frequent_shop_text")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Your most frequent shop"

def highestVendors_text():
    texts = Constant.objects.filter(key="highest_vendors_text")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Watch those spendings"

def dailyAverage_text():
    texts = Constant.objects.filter(key="daily_average_text")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Your daily average spend"

def lastMonthMore_text():
    texts = Constant.objects.filter(key="last_month_more_text")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "You have spent more than last month"

def lastMonthLess_text():
    texts = Constant.objects.filter(key="last_month_less_text")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "You have spent less than last month"

def prediction_text():
    texts = Constant.objects.filter(key="prediction_text")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "You are predicted to spend next month"


def newUser_success():
    texts = Constant.objects.filter(key="new_user_success")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "New User Created."

def pin_success():
    texts = Constant.objects.filter(key="pin_success")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Pin has been updated."

def userFound_success():
    texts = Constant.objects.filter(key="user_found_success")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "User Found."

def forgetPin_success():
    texts = Constant.objects.filter(key="forget_pin_success")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Code Sent successfully."

def newPot_success():
    texts = Constant.objects.filter(key="new_pot_success")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "New Pot Created."

def deletePot_success():
    texts = Constant.objects.filter(key="delete_pot_success")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Pot has been deleted."

def updatePot_success():
    texts = Constant.objects.filter(key="update_pot_success")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Pot has been updated."

def userUpdated_success():
    texts = Constant.objects.filter(key="user_updated_success")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "User Updated."

def lowBanalce_tip():
    texts = Constant.objects.filter(key="low_balance_tip")
    if texts.count() > 0:
        text = list(texts)[0]
        return text.text
    return "Slow down on the spending your bank balance is running low!"