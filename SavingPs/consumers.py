from .models import Ubuntu, User, ChatMessage, Record
import json
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.db import database_sync_to_async

class shiftRequest(AsyncWebsocketConsumer):
    async def connect(self):
        
        self.sender_id = self.scope['url_route']['kwargs']['sender_id']
        self.rec_id = self.scope['url_route']['kwargs']['rec_id']
        self.room_group_name = f"shiftRequest-{self.sender_id}-{self.rec_id}"

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        pass

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        await self.channel_layer.group_send(
            self.room_group_name,
            {
                "message": message,
                "sender_channel_name": self.channel_name,
                "type": "shifRequestMessage",
            }
        )

    async def shifRequestMessage(self, event):
        message = event['message']

        if self.channel_name != event['sender_channel_name']:
            await self.send(text_data=json.dumps({
                'message': message,
            }))



class groupChat(AsyncWebsocketConsumer):


    @database_sync_to_async
    def save_chat_message(self, user_id, msg_type, message):
        ubuntu = Ubuntu.objects.filter(id=self.ubuntu_id).first()
        msg_sender = User.objects.filter(id=user_id).first()
        if ubuntu and msg_sender:
            if msg_type == "text":
                ChatMessage.objects.create(
                    ubuntu = ubuntu,
                    msg_sender = msg_sender,
                    message = message,
                    type = msg_type,
                )
            elif msg_type == "record":
                ChatMessage.objects.create(
                    ubuntu = ubuntu,
                    msg_sender = msg_sender,
                    record = Record.objects.filter(id=message).first(),
                    type = msg_type,
                )
        return True


    async def connect(self):
        
        self.ubuntu_id = self.scope['url_route']['kwargs']['ubuntu_id']
        self.room_group_name = f"groupChat-{self.ubuntu_id}"

        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        pass

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']
        msg_type = text_data_json['msg_type']
        user_id = text_data_json['user_id']
        user_name = text_data_json['user_name']

        saving_chat_message = await self.save_chat_message(user_id, msg_type, message)

        await self.channel_layer.group_send(
            self.room_group_name,
            {
                "message": message,
                "user_id": user_id,
                "user_name": user_name,
                "sender_channel_name": self.channel_name,
                "type": "groupChatMessage",
            }
        )

    async def groupChatMessage(self, event):
        message = event['message']
        user_id = event['user_id']
        user_name = event['user_name']

        if self.channel_name != event['sender_channel_name']:
            await self.send(text_data=json.dumps({
                "message": message,
                "user_id": user_id,
                "user_name": user_name,
            }))


