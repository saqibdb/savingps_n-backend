from __future__ import absolute_import, unicode_literals
from celery import shared_task
from datetime import datetime
from .Utility import send_required_notifications


@shared_task()
def task_send_required_notifications(myarg):
    send_required_notifications()
    print("Email Sent Successfully")
