"""SavingPs_N URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from SavingPs import views
from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^sign_up/$', views.signUp),
    url(r'^verify_code/$', views.verifyCode),
    url(r'^set_pin/$', views.setPin),
    url(r'^logIn/$', views.logIn),
    url(r'^forget_pin/$', views.forgetPin),
    url(r'^get_transactions/$', views.getTransactions),
    url(r'^get_transactioned_categories/$', views.getTransactionedCategories),
    url(r'^get_all_transactions_of_category/$', views.getAllTransactionsOfCategory),
    url(r'^get_highest_retailer/$', views.getHighestRetailer),
    url(r'^get_most_frequent_shop/$', views.getMostFrequentShop),
    url(r'^get_top_shops/$', views.getHihestVendors),
    url(r'^get_daily_average_spend/$', views.getDailyAverageSpend),
    url(r'^get_last_month_spend/$', views.getLastMonthSpend),
    url(r'^get_all_categories/$', views.getAllCategories),
    url(r'^assign_category_to_transaction/$', views.assignCategoryToTransaction),
    url(r'^get_all_tips/$', views.getAllTips),
    url(r'^get_all_insights/$', views.getAllInsights),
    url(r'^create_new_pot/$', views.createNewPot),
    url(r'^get_all_user_pots/$', views.getAllUserPots),
    url(r'^delete_pot/$', views.deletePot),
    url(r'^update_pot/$', views.updatePot),
    url(r'^update_user/$', views.UpdateUser),
    url(r'^update_user_image/$', views.updateUserImage),
    url(r'^update_user_image_b64/$', views.updateUserImageB64),
    url(r'^get_weekly_spend/$', views.getWeeklySpend),
    url(r'^get_prediction/$', views.getPrediction),
    url(r'^get_prediction_test/$', views.getPredictionTest),
    url(r'^get_balance/$', views.getBalance),
    url(r'^make_all_vendors_retailers/$', views.makeAllVendorsRetailers),
    url(r'^get_savings/$', views.getAllSavingsFromPots),
    url(r'^get_transactions_without_category/$', views.getTransactionsWithoutCategory),
                  #url(r'^add_vendor/$', views.addVendor),
    #url(r'^fix_duplicate_vendors/$', views.fixDuplicateVendors),
    #url(r'^fix_vendor_with_name/$', views.fixVendorWithName),
    #url(r'^fix_transaction_category/$', views.fixTransactionCategory),
    url(r'^get_twilio_recording/$', views.getTwilioRecording),
    url(r'^get_twilio_status/$', views.getTwilioCallStatus),
    url(r'^calculate_pot_saving/$', views.calculatePotSaving),
    url(r'^get_savings_this_month/$', views.getSavingThisMonth),
    url(r'^get_all_pot_transactions/$', views.getAllPotTransactions),

    url(r'^save_monthly_debit/$', views.saveMonthlyDebit),
    url(r'^get_monthly_debit/$', views.getMonthlyDebit),

    url(r'^save_monthly_credits/$', views.saveMonthlyCredits),
    url(r'^register_user_device_token/$', views.registerUserDeviceToken),
    url(r'^get_all_user_ubuntus/$', views.getAllUserUbuntus),
    url(r'^save_ubuntu/$', views.saveUbuntu),
    url(r'^start_ubuntu/$', views.startUbuntu),
    url(r'^send_ubuntu_invitation/$', views.sendUbuntuInvitation),
    url(r'^get_ubuntu_invitations/$', views.getUbuntuInvitations),
    url(r'^answer_ubuntu_invitation/$', views.answerUbuntuInvitation),
    url(r'^send_ubuntu_transaction/', views.sendUbuntuTransaction),
    url(r'^receive_ubuntu_transaction/', views.receiveUbuntuTransaction),
    url(r'^send_ubuntu_shift_request/$', views.sendUbuntuShiftRequest),
    url(r'^get_ubuntu_shift_requests/$', views.getUbuntuShiftRequests),
    url(r'^answer_ubuntu_shift_request/$', views.answerUbuntuShiftRequest),
    url(r'^get_all_users_for_invite/$', views.getAllUsersForInvite),
    url(r'^upload_chat_record/$', views.uploadChatRecord),
    url(r'^get_chat_history/$', views.getChatHistory),
    url(r'^test_users/$', views.testUsers),


] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)